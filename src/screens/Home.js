import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, ImageBackground, FlatList, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
import { get } from 'lodash';
import HomeCardView from '../components/common/HomeCardView';
import RecommendUserList from '../components/common/RecommendUserList';
import { images, Metrics, Colors } from '../assets/index';

const items = [
    {
        firstName: 'Hariom',
        lastName: 'Raje'
    },
    {
        firstName: 'Rohan',
        lastName: 'Mali',
    },
    {
        firstName: 'Nitin',
        lastName: 'Ghadiya',
    },
    {
        firstName: 'Monica',
        lastName: 'Vekariya',
    }
];

const slide = [
    {
        image: images.HomeImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.HomeImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.HomeImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.HomeImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
]

class Home extends React.Component {

    askForRecommendation() {
        this.props.navigation.navigate('AskForRecommendation');
    }


    renderGroupPost = () => {
        return (
            <View style={styles.viewStyle}>
                <TouchableOpacity onPress={() => this.askForRecommendation()}>
                    <Image source={images.AskRecommendationLogo} resizeMode='cover' style={{ height: Metrics.CountScale(65), width: Metrics.CountScale(65) }} />
                </TouchableOpacity>
                <View style={{ marginTop: Metrics.CountScale(10) }}>
                    <Text numberOfLines={1} style={styles.textStyle}>Ask my friend</Text>
                </View>
            </View>
        );
    };

    render() {
        return (
            <ScrollView bounces={false} style={styles.container}>
                <View>
                    <Swiper
                        style={{ height: Metrics.stopperImage }}
                        showsButtons={false}
                        autoplay={true}
                        dot={<Image source={images.DotIcon} resizeMode={'contain'} style={styles.dotStyle} />}
                        activeDot={<Image source={images.ActiveSlideIcon} resizeMode={'contain'} style={styles.dotStyle} />}
                    >
                        {slide.map(inboxObj => (
                            <ImageBackground source={inboxObj.image} style={{ width: Metrics.screenWidth, height: Metrics.stopperImage }} resizeMode={'cover'} >

                            </ImageBackground>
                        ))}
                    </Swiper>
                    <View style={styles.recommendListView}>
                        {items.map((inboxObj, index) => (
                            <TouchableOpacity style={{ width: Metrics.CountScale(50), marginRight: Metrics.CountScale(-27), zIndex: -index + 4, height: Metrics.CountScale(30) }}>
                                {index <= 2 &&
                                    <View style={styles.commonAbsolute}>
                                        <Image source={images.HomeImage} resizeMode={'cover'} style={styles.recommendList} />
                                    </View>
                                }
                            </TouchableOpacity>
                        ))}
                        <TouchableOpacity style={{ width: Metrics.CountScale(30), marginRight: Metrics.CountScale(15), zIndex: 1, height: Metrics.CountScale(30) }}>
                            {items.length > 3 &&
                                <View style={[styles.commonAbsolute, { borderRadius: Metrics.CountScale(30) / 2 }]}>
                                    <Image source={images.SettingsImage} resizeMode={'cover'} style={{ width: Metrics.CountScale(30), height: Metrics.CountScale(30) }} />
                                </View>
                            }
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1, paddingTop: Metrics.CountScale(20) }}>
                    <Text style={{ color: Colors.BLACK, fontSize: Metrics.CountScale(15), paddingLeft: Metrics.CountScale(10) }}>Asks for Recommendations</Text>
                    <FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item, index }) => <RecommendUserList data={item} index={index} navigate={get(this.props, 'navigation', null)} />}
                        ListHeaderComponent={this.renderGroupPost}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <View style={styles.flatlistView}>
                    <Text style={styles.restaurantPlaceText}>Special Offers</Text>
                    <FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) => <HomeCardView data={item} navigate={get(this.props, 'navigation', null)} />}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <View style={styles.flatlistView}>
                    <Text style={styles.restaurantPlaceText}>Places I Want To Go</Text>
                    <FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) => <HomeCardView data={item} navigate={get(this.props, 'navigation', null)} />}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <View style={styles.flatlistView}>
                    <Text style={styles.restaurantPlaceText}>Most Visited Places</Text>
                    <FlatList
                        data={items}
                        horizontal={true}
                        renderItem={({ item }) => <HomeCardView data={item} navigate={get(this.props, 'navigation', null)} />}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
            </ScrollView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    dotStyle: {
        margin: Metrics.CountScale(5),
        bottom: Metrics.CountScale(-15)
    },
    textStyle: {
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(12),
        width: Metrics.CountScale(90),
        textAlign: 'center'
    },
    viewStyle: {
        marginVertical: Metrics.CountScale(10),
        alignItems: 'center',
        marginLeft: Metrics.CountScale(16)
    },
    flatlistView: {
        flex: 1,
        paddingTop: Metrics.CountScale(15)
    },
    restaurantPlaceText: {
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(15),
        paddingLeft: Metrics.CountScale(10)
    },
    recommendUserImageList: {
        height: Metrics.CountScale(65),
        width: Metrics.CountScale(65),
        borderRadius: Metrics.CountScale(65) / 2,
        borderWidth: 3,
        borderColor: Colors.WHITE
    },
    recommendListView: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom: Metrics.CountScale(10),
        right: Metrics.CountScale(0),
        height: Metrics.CountScale(30)
    },
    recommendList: {
        width: Metrics.CountScale(30),
        height: Metrics.CountScale(30),
        borderColor: Colors.WHITE,
        borderWidth: 1,
        borderRadius: Metrics.CountScale(30) / 2
    },
    commonAbsolute: {
        position: 'absolute',
        bottom: Metrics.CountScale(0),
        right: Metrics.CountScale(0)
    },
});

export default Home;