import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Platform } from 'react-native';
import Contacts from 'react-native-contacts';
import { Metrics, Colors, images } from '../assets/index';

class FriendsModule extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Friends',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    state = {
        contactList: []
    };

    async componentDidMount() {
        self = this;
        let mobileContact = [];

        this.setState({ loading: true });

        if (Platform.OS == 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'This app would like to view your contacts.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied') {
                        console.log(err);
                    } else {
                        console.log(contacts, 'contacts')
                        contacts.forEach(element => {
                            if (element.phoneNumbers.length != 0) {
                                mobileContact.push({ name: element.givenName, number: element.phoneNumbers[0].number.replace(/\D/g, '') })
                            }
                        });
                        // this.displayNumber(mobileContact);
                        console.log(mobileContact, 'mobileContact');
                    }
                })
            })
        } else {
            Contacts.getAll((err, contacts) => {
                if (err === 'denied') {
                    console.log(err);
                } else {
                    console.log(contacts, 'contacts')
                    contacts.forEach(element => {
                        if (element.phoneNumbers.length != 0) {
                            mobileContact.push({ name: element.givenName, number: element.phoneNumbers[0].number.replace(/\D/g, '') })
                        }
                    });
                    // this.displayNumber(mobileContact);
                    console.log(mobileContact, 'mobileContact');
                }
            })
        }
    }

    // displayNumber(mobileContact) {
    //     let array = [];
    //     let dbContact = [];

    //     const data = {
    //         "is_testdata": "1",
    //         "device_token": global.fcmDeviceToken,
    //         "device_type": Platform.OS === 'ios' ? 0 : 1,
    //         "secret_key": tempToken,
    //         "access_key": "nousername",
    //         "group_id": this.state.screen == "GroupProfile" ? this.state.result.id : "0"
    //     };
    //     console.log(data)
    //     const body = JSON.stringify(data);

    //     APICaller('GetUserNumber', 'POST', body).then(async (json) => {
    //         console.log(json, 'GetUserNumber');
    //         if (json.status == 1) {
    //             dbContact = json.data.User;
    //             await this.setState({ loading: false });

    //             dbContact.forEach(val => {
    //                 mobileContact.forEach(element => {
    //                     if (element.number == val.phone_number && val.user_id != userId.userId) {
    //                         array.push({ name: element.name, number: element.number, status: val.status, id: val.user_id })
    //                     }
    //                     this.setState({ contactList: array });
    //                 });
    //             });

    //             console.log(this.state.contactList);

    //         } else {
    //             this.setState({ loading: false });
    //             alert('Something went wrong');
    //         }
    //     })
    //         .catch((error) => {
    //             this.setState({ loading: false });
    //             alert('Something went wrong, try again.')
    //             console.error(error);
    //         });

    // }

    render() {
        return (
            <View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center'
    }
});

export default FriendsModule;
