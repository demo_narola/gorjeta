import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

class Add extends React.Component {

    auth() {
        alert('Add');
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.auth()}>
                    <Text>
                        Add
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default Add;