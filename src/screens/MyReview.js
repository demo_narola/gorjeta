import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import StarRating from 'react-native-star-rating';
import { EventRegister } from 'react-native-event-listeners';
import { Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let access_Token;
let body = '';
let header = '';
let token = '';

class MyReview extends React.Component {

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Review',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    state = {
        comment: '',
        foodStar: 0,
        priceStar: 0,
        environmentStar: 0,
        waiterStar: 0,
        loading: false
    }

    async foodStarRating(rating) {
        await this.setState({ foodStar: rating });
    }

    async priceStarRating(rating) {
        await this.setState({ priceStar: rating });
    }

    async environmentStarRating(rating) {
        await this.setState({ environmentStar: rating });
    }

    async waiterStarRating(rating) {
        await this.setState({ waiterStar: rating });
    }

    async resetData() {
        await this.setState({ comment: '', foodStar: 0, priceStar: 0, environmentStar: 0, waiterStar: 0 });
    }

    async saveReview() {

        if (this.state.comment.trim() == '') {
            alert('Please enter comment.')
        }
        else if (this.state.foodStar == 0) {
            alert('Please rate for food.')
        }
        else if (this.state.priceStar == 0) {
            alert('Please rate for price.')
        }
        else if (this.state.environmentStar == 0) {
            alert('Please rate for environment.')
        }
        else if (this.state.waiterStar == 0) {
            alert('Please rate for waiter.')
        }
        else {
            this.setState({ loading: true });

            token = 'Bearer ' + access_Token;

            const data = {
                "description": this.state.comment,
                "environment": this.state.environmentStar,
                "food": this.state.foodStar,
                "price": this.state.priceStar,
                "waiterService": this.state.waiterStar,
                "userId": 178,
                "restaurantId": 500
            };

            body = JSON.stringify(data);

            console.log(body);

            APICaller('users/api/v1/users/review-restaurant', 'post', token, body, header).then(async (json) => {
                this.setState({ loading: false });
                await EventRegister.emit('UpdateReview');
                this.props.navigation.goBack();
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView
                    bounces={false}
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View style={{ paddingHorizontal: Metrics.CountScale(20), paddingVertical: Metrics.CountScale(10) }}>
                        <Text style={{ fontSize: Metrics.CountScale(20), fontWeight: 'bold' }}>
                            Comment
                    </Text>
                    </View>

                    <View style={styles.textInputViewStyle}>
                        <TextInput
                            multiline
                            textAlignVertical={'top'}
                            placeholder={'Enter Review'}
                            placeholderTextColor={Colors.GRAY}
                            autoFocus={false}
                            value={this.state.comment}
                            onChangeText={val => this.setState({ comment: val })}
                            style={styles.textInputStyle}
                        />
                    </View>

                    <View style={{ paddingHorizontal: Metrics.CountScale(20), paddingVertical: Metrics.CountScale(10) }}>
                        <Text style={{ fontSize: Metrics.CountScale(20), fontWeight: 'bold' }}>
                            Rating
                    </Text>
                    </View>


                    <View style={styles.evaluationRatingInnerViewSection}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <StarRating
                                rating={this.state.foodStar}
                                disabled={false}
                                maxStars={5}
                                starSize={20}
                                starStyle={{ marginHorizontal: Metrics.CountScale(3) }}
                                fullStar={'star'}
                                fullStarColor={Colors.ORANGE}
                                selectedStar={(rating) => this.foodStarRating(rating)}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text numberOfLines={1} style={{ fontWeight: '500', color: Colors.BLACK, fontSize: Metrics.CountScale(17), marginLeft: Metrics.CountScale(27) }}>Food</Text>
                        </View>
                    </View>

                    <View style={styles.evaluationRatingInnerViewSection}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <StarRating
                                rating={this.state.priceStar}
                                disabled={false}
                                maxStars={5}
                                starSize={20}
                                starStyle={{ marginHorizontal: Metrics.CountScale(3) }}
                                fullStar={'star'}
                                fullStarColor={Colors.ORANGE}
                                selectedStar={(rating) => this.priceStarRating(rating)}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text numberOfLines={1} style={{ fontWeight: '500', color: Colors.BLACK, fontSize: Metrics.CountScale(17), marginLeft: Metrics.CountScale(27) }}>Price</Text>
                        </View>
                    </View>

                    <View style={styles.evaluationRatingInnerViewSection}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <StarRating
                                rating={this.state.environmentStar}
                                disabled={false}
                                maxStars={5}
                                starSize={20}
                                starStyle={{ marginHorizontal: Metrics.CountScale(3) }}
                                fullStar={'star'}
                                fullStarColor={Colors.ORANGE}
                                selectedStar={(rating) => this.environmentStarRating(rating)}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text numberOfLines={1} style={{ fontWeight: '500', color: Colors.BLACK, fontSize: Metrics.CountScale(17), marginLeft: Metrics.CountScale(27) }}>Environment</Text>
                        </View>
                    </View>

                    <View style={styles.evaluationRatingInnerViewSection}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <StarRating
                                rating={this.state.waiterStar}
                                disabled={false}
                                maxStars={5}
                                starSize={20}
                                starStyle={{ marginHorizontal: Metrics.CountScale(3) }}
                                fullStar={'star'}
                                fullStarColor={Colors.ORANGE}
                                selectedStar={(rating) => this.waiterStarRating(rating)}
                            />
                        </View>
                        <View style={{ flex: 1 }}>
                            <Text numberOfLines={1} style={{ fontWeight: '500', color: Colors.BLACK, fontSize: Metrics.CountScale(17), marginLeft: Metrics.CountScale(27) }}>Waiter Service</Text>
                        </View>
                    </View>

                    <View style={styles.viewStyle}>
                        <TouchableOpacity onPress={() => this.resetData()} style={[styles.touchableStyle, { marginRight: Metrics.CountScale(5) }]}>
                            <Text style={styles.buttonTextStyle}>Reset</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.saveReview()} style={[styles.touchableStyle, { marginLeft: Metrics.CountScale(5) }]}>
                            <Text style={styles.buttonTextStyle}>Save</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(20),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    textInputViewStyle: {
        paddingHorizontal: Metrics.CountScale(20),
        paddingVertical: Metrics.CountScale(10),
        justifyContent: 'center',
    },
    textInputStyle: {
        padding: Metrics.CountScale(15),
        borderRadius: Metrics.CountScale(15),
        height: Metrics.CountScale(150),
        borderWidth: 1,
        borderColor: Colors.GRAY,
        color: Colors.GRAY,
        fontSize: Metrics.CountScale(15)
    },
    viewStyle: {
        paddingHorizontal: Metrics.CountScale(20),
        paddingTop: Metrics.CountScale(40),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    touchableStyle: {
        backgroundColor: Colors.NAVIGATIONLABEL,
        height: Metrics.CountScale(40),
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Metrics.CountScale(20)
    },
    buttonTextStyle: {
        fontSize: Metrics.CountScale(17),
        fontWeight: '500',
        color: Colors.WHITE
    },
    evaluationRatingInnerViewSection: {
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 10,
        paddingVertical: 15,
        justifyContent: 'center'
    }
});

export default MyReview;