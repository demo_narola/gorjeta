import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ImageBackground, Image, Platform, TextInput, AsyncStorage } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import { images, Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let header = '';
let token = '';

class Login extends React.Component {

    state = {
        email_id: '',
        password: '',
        loading: false,
        show: false
    };

    async componentDidMount() {
        await AsyncStorage.getItem('REMEMBER_ME', async (err, jsonResult) => {
            if (jsonResult) {
                const rememberMe = JSON.parse(jsonResult);
                this.setState({ email_id: rememberMe.email, password: rememberMe.password, show: true })
            }
        });
    }

    login() {

        if (this.state.email_id.trim() == '') {
            alert('Please enter email.')
        }
        else if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email_id) == false) {
            alert('Please enter valid email.')
        }
        else if (this.state.password.trim() == '') {
            alert('Please enter password.')
        }
        else if (this.state.password.length < 6 || this.state.password.length > 16) {
            alert('Password should be at least 6-15 characters.')
        }
        else {

            this.setState({ loading: true });

            var formData = new FormData();

            formData.append('username', this.state.email_id);
            formData.append('password', this.state.password);
            formData.append('grant_type', 'password');

            APICaller('auth/login', 'post', token, formData, header).then(async (json) => {
                console.log(json, '!!!!!');
                if (json.status == 'error') {
                    alert(json.message);
                }
                else {
                    await this.setState({ result: json });

                    AsyncStorage.setItem('ACCESS_TOKEN', json.accessToken);
                    AsyncStorage.setItem('USER_ID', JSON.stringify({ user_id: json.id }));

                    if (this.state.show == true) {
                        AsyncStorage.setItem('REMEMBER_ME', JSON.stringify({ email: this.state.email_id, password: this.state.password }));
                    }
                    else {
                        this.setState({ email_id: '', password: '' });
                    }
                    this.props.navigation.navigate('TabBar');
                }
                await this.setState({ loading: false });
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }
    registration() {
        this.props.navigation.navigate('Registration');
    }
    facebookLogin() {
        this.props.navigation.navigate('TabBar');
    }
    gmailLogin() {
        this.props.navigation.navigate('TabBar');
    }
    forgotPassword() {
        this.props.navigation.navigate('ForgotPassword');
    }

    remberPassword() {
        if (this.state.show == false) {
            this.setState({ show: true });
        }
        else {
            AsyncStorage.removeItem('REMEMBER_ME');
            this.setState({ show: false });
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={images.BlurBackground} style={{ flex: 1 }} resizeMode={'cover'} >
                    <KeyboardAwareScrollView
                        style={{ flex: 1 }}
                    >
                        <View style={{ marginTop: Metrics.CountScale(78), alignItems: 'center', justifyContent: 'center', paddingTop: Platform.OS == 'ios' ? (Metrics.screenHeight == 812 ? 44 : 20) : 0 }}>
                            <Image source={images.AppLogo} resizeMode={'cover'} />
                        </View>

                        <View style={{ padding: Metrics.CountScale(25), marginTop: Metrics.CountScale(20) }}>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', borderRadius: Metrics.CountScale(40) }}>
                                <TextInput
                                    placeholder="EMAIL"
                                    placeholderTextColor={Colors.WHITE}
                                    selectionColor={Colors.WHITE}
                                    style={styles.textinputStyle}
                                    value={this.state.email_id}
                                    onChangeText={val => this.setState({ email_id: val })}
                                />
                            </View>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', borderRadius: Metrics.CountScale(40), marginTop: Metrics.CountScale(15) }}>
                                <TextInput
                                    placeholder="PASSWORD"
                                    placeholderTextColor={Colors.WHITE}
                                    selectionColor={Colors.WHITE}
                                    style={styles.textinputStyle}
                                    secureTextEntry
                                    value={this.state.password}
                                    onChangeText={val => this.setState({ password: val })}
                                />
                            </View>
                            <TouchableOpacity onPress={() => { this.login() }} style={[styles.loginButtonstyle, { marginTop: Metrics.CountScale(15) }]}>
                                <Text style={{ fontSize: Metrics.CountScale(15), }}>
                                    LOG IN
                            </Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.forgotPassword() }} style={{ padding: Metrics.CountScale(10), marginTop: Metrics.CountScale(10), justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: Metrics.CountScale(15), color: Colors.WHITE }}>
                                    Forget your password?
                            </Text>
                            </TouchableOpacity>

                            <View style={{ flexDirection: "row", padding: Metrics.CountScale(10), justifyContent: 'center', alignItems: 'center' }}>
                                <TouchableOpacity onPress={() => this.remberPassword()} style={{ width: Metrics.CountScale(20) }}>
                                    {this.state.show == 0 ? <Icon name="square-o" size={17} color={Colors.WHITE} /> : <Icon name="check-square-o" size={17} color={Colors.WHITE} />}
                                </TouchableOpacity>
                                <Text style={styles.txtStyle3}>
                                    Remember Me
                                </Text>
                            </View>
                        </View>
                        <View style={{ marginTop: Metrics.CountScale(140) }}>
                            <View style={styles.buttonSectionBottom}>
                                <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, { marginRight: Metrics.CountScale(10) }]}>
                                    <Image source={images.FbButton} resizeMode={'cover'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, { marginLeft: Metrics.CountScale(10) }]}>
                                    <Image source={images.GmailButton} resizeMode={'cover'} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                <Text style={{ color: Colors.WHITE, fontSize: Metrics.CountScale(15) }}>Don't have an account?</Text>
                                <TouchableOpacity onPress={() => { this.registration() }} style={{ padding: Metrics.CountScale(10) }}>
                                    <Text style={{ color: Colors.SIGNUP, fontWeight: 'bold' }}>SIGN UP</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textinputStyle: {
        paddingTop: Metrics.CountScale(0),
        paddingBottom: Metrics.CountScale(0),
        height: Metrics.CountScale(50),
        width: '100%',
        textAlign: 'center',
        color: Colors.WHITE,
        borderRadius: Metrics.CountScale(40),
        paddingHorizontal: Metrics.CountScale(10),
        fontSize: Metrics.CountScale(15),
    },
    loginButtonstyle: {
        backgroundColor: Colors.WHITE,
        height: Metrics.CountScale(45),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Metrics.CountScale(40)
    },
    buttonSectionBottom: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Metrics.CountScale(32),
        paddingBottom: Metrics.CountScale(15),
        alignItems: 'flex-end'
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(45),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    txtStyle3: {
        color: Colors.WHITE,
        fontSize: Metrics.CountScale(15),
        alignSelf: 'center',
        marginLeft: Metrics.CountScale(5)
    },
});

export default Login;