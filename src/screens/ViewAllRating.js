import React from 'react';
import { Text, View, StyleSheet, FlatList, Image, AsyncStorage } from 'react-native';
import StarRating from 'react-native-star-rating';
import { Metrics, Colors, images } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let access_Token;
let body = '';
let header = '';
let token = '';

class ViewAllRating extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Rating',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    state = {
        result: [],
        loading: false
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.getUsersRating();

        setTimeout(() => {
            this.setState({ loading: false });
        }, 1500);
    }

    async getUsersRating() {

        this.setState({ loading: true });

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/restautant-reviews/500', 'get', token, body, header).then(async (json) => {
            await this.setState({ result: json, loading: false });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ marginBottom: 10 }}>
                    <FlatList
                        data={this.state.result}
                        showsVerticalScrollIndicator={false}
                        renderItem={({ item }) => (
                            <View style={[styles.shadowEffect, styles.evaluationRatingSection]}>
                                <View style={{ flexDirection: 'row', paddingHorizontal: Metrics.CountScale(15), paddingVertical: Metrics.CountScale(10) }}>
                                    <View>
                                        <Image source={images.Offermage} resizeMode='cover' style={styles.ratedUserImageStyle} />
                                    </View>
                                    <View style={{ marginLeft: Metrics.CountScale(10), justifyContent: 'center', flex: 1 }}>
                                        <Text style={{ fontSize: Metrics.CountScale(16), fontWeight: 'bold' }} numberOfLines={1}>
                                            {item.user.firstname} {item.user.lastName}
                                        </Text>
                                    </View>
                                </View>

                                <View style={{ paddingHorizontal: Metrics.CountScale(15), paddingVertical: Metrics.CountScale(10) }}>
                                    <View>
                                        <Text style={{ fontSize: Metrics.CountScale(13) }} numberOfLines={6}>
                                            {item.description}
                                        </Text>
                                    </View>
                                </View>

                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={item.food}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.commonTextStyle}>Food</Text>
                                    </View>
                                </View>

                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={item.price}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.commonTextStyle}>Price</Text>
                                    </View>
                                </View>
                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={item.environment}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.commonTextStyle}>Environment</Text>
                                    </View>
                                </View>

                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={item.waiterService}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={styles.commonTextStyle}>Waiter Service</Text>
                                    </View>
                                </View>
                            </View>
                        )}
                    />
                </View>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(20),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    evaluationRatingInnerViewSection: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginVertical: Metrics.CountScale(13)
    },
    ratedUserImageStyle: {
        borderRadius: Metrics.CountScale(45) / 2,
        width: Metrics.CountScale(45),
        height: Metrics.CountScale(45),
        borderWidth: 1
    },
    commonTextStyle: {
        fontSize: Metrics.CountScale(14),
        fontWeight: 'bold'
    },
    evaluationRatingSection: {
        flex: 1,
        marginHorizontal: Metrics.CountScale(20),
        marginTop: Metrics.CountScale(25),
        borderRadius: Metrics.CountScale(10),
        backgroundColor: 'white'
    },
    shadowEffect: {
        shadowOpacity: 1,
        shadowOffset: { height: 4, width: 0 },
        elevation: 5,
        shadowColor: 'gray',
        shadowRadius: 5
    },
});

export default ViewAllRating;