import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import { Metrics, Colors } from '../assets/index';
import { TextField } from 'react-native-material-textfield';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let access_Token;
let header = '';
let token = '';

class ChangePassword extends Component {

    state = {
        oldPassword: '',
        newPassword: '',
        confirmPassword: '',
        loading: false
    };

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Change Password',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    back() {
        this.props.navigation.goBack();
    }

    changePassword() {

        if (this.state.oldPassword.trim() == '') {
            alert('Please enter old password.')
        }
        else if (this.state.newPassword.trim() == '') {
            alert('Please enter new Password.')
        }
        else if (this.state.newPassword.length < 6 || this.state.newPassword.length > 16) {
            alert('New Password should be at least 6-15 characters.')
        }
        else if (this.state.confirmPassword.trim() == '') {
            alert('Please enter confirm Password.')
        }
        else if (this.state.confirmPassword.length < 6 || this.state.confirmPassword.length > 16) {
            alert('Confirm password should be at least 6-15 characters.')
        }
        else if (this.state.newPassword !== this.state.confirmPassword) {
            alert('New Password and Confirm Password does not match.')
        }
        else {
            this.setState({ loading: true });

            token = 'Bearer ' + access_Token;

            var formData = new FormData();

            formData.append("oldPassword", this.state.oldPassword);
            formData.append("newPassword", this.state.newPassword);

            APICaller('users/api/v1/users/reset-password', 'put', token, formData, header).then(async (json) => {
                console.log(json);
                this.setState({ loading: false });

                if (json.status == 'success') {
                    Alert.alert(
                        '',
                        'Password Changed successfully.',
                        [
                            { text: 'Ok', onPress: () => { this.back() } },
                        ],
                        { cancelable: false },
                    );
                }
                else {
                    alert(json.message);
                }
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }} >
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                    keyboardShouldPersistTaps={'handled'}
                >
                    <View style={{ padding: Metrics.CountScale(25) }}>
                        <TextField
                            label='Old Password'
                            textColor={Colors.BLACK}
                            fontSize={Metrics.CountScale(18)}
                            tintColor={Colors.NAVIGATIONLABEL}
                            autoFocus={false}
                            value={this.state.oldPassword}
                            onChangeText={val => this.setState({ oldPassword: val })}
                        />
                        <TextField
                            label='New Password'
                            textColor={Colors.BLACK}
                            fontSize={Metrics.CountScale(18)}
                            tintColor={Colors.NAVIGATIONLABEL}
                            autoFocus={false}
                            value={this.state.newPassword}
                            onChangeText={val => this.setState({ newPassword: val })}
                        />
                        <TextField
                            label='Confirm Password'
                            textColor={Colors.BLACK}
                            fontSize={Metrics.CountScale(18)}
                            tintColor={Colors.NAVIGATIONLABEL}
                            autoFocus={false}
                            value={this.state.confirmPassword}
                            onChangeText={val => this.setState({ confirmPassword: val })}
                        />
                        <TouchableOpacity onPress={() => { this.changePassword() }} style={styles.changePasswordButtonStyle}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={styles.changePasswordTextStyle}>Change Password</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View >
        )
    }
}

const styles = {
    changePasswordTextStyle: {
        color: Colors.WHITE,
        fontSize: Metrics.CountScale(20)
    },
    changePasswordButtonStyle: {
        width: '100%',
        height: Metrics.CountScale(45),
        backgroundColor: Colors.NAVIGATIONLABEL,
        borderRadius: Metrics.CountScale(25),
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: Metrics.CountScale(40)
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    profileImageSize: {
        height: Metrics.CountScale(110),
        width: Metrics.CountScale(110),
        borderRadius: Metrics.CountScale(110 / 2)
    },
    profileViewImageSize: {
        height: Metrics.CountScale(110),
        width: Metrics.CountScale(110),
        borderRadius: Metrics.CountScale(110 / 2),
        backgroundColor: '#e2e2e2',
        alignItems: 'center',
        alignSelf: 'center'
    },
    commonTextStyle: {
        marginTop: Metrics.CountScale(10),
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(16)
    }
}

export default ChangePassword;
