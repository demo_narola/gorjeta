import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

class EvaluationViewAll extends React.Component {

    auth() {
        alert('EvaluationViewAll');
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.auth()}>
                    <Text>
                        EvaluationViewAll
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'orange',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export default EvaluationViewAll;