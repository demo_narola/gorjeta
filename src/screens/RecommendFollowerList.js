import React from 'react';
import { Text, SafeAreaView, StyleSheet, FlatList, TouchableOpacity, View, Alert, Image, AsyncStorage } from 'react-native';
import { Metrics, Colors, images } from '../assets/index';
import { EventRegister } from 'react-native-event-listeners';
import Icon from 'react-native-vector-icons/FontAwesome';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let access_Token;
let body = '';
let header = '';
let token = '';

class RecommendFollowerList extends React.Component {

    state = {
        result: [],
        loading: false,
        selectedFollowerId: ''
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Followers',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.RECOMMENDEDBUTTON },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.getFollowerList();
    }

    back() {
        this.props.navigation.goBack();
    }

    async getFollowerList() {

        // const endPoint = `users/${56}/followers`;

        token = 'Bearer ' + access_Token;

        this.setState({ loading: true });

        APICaller('users/api/v1/users/56/followers', 'get', token, body, header).then(async (json) => {
            console.log(json[0].firstName);
            await this.setState({ result: json, loading: false })
            console.log(this.state.result);
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    recommendRestaurant() {

        token = 'Bearer ' + access_Token;

        this.setState({ loading: true });

        var formData = new FormData();

        if (this.state.selectedFollowerId != '') {

            formData.append("recommendedBy", 56);
            formData.append("restaurantId", 4);
            formData.append("recommendedTo", 55);

            APICaller('users/api/v1/users/recommends-restaurant', 'post', token, formData, 'multipart/form-data', header).then(async (json) => {
                console.log(json);
                this.setState({ loading: false });
                await EventRegister.emit('UpdateProfile');
                Alert.alert(
                    '',
                    'Restaurant Recommended successfully.',
                    [
                        { text: 'Ok', onPress: () => { this.back() } },
                    ],
                    { cancelable: false },
                );
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        } else {
            formData.append("recommendedBy", 56);
            formData.append("restaurantId", 4);

            APICaller('users/api/v1/users/recommends-restaurant', 'post', token, formData, 'multipart/form-data', header).then(async (json) => {
                console.log(json);
                this.setState({ loading: false });
                await EventRegister.emit('UpdateProfile');
                Alert.alert(
                    '',
                    'Restaurant Recommended successfully.',
                    [
                        { text: 'Ok', onPress: () => { this.back() } },
                    ],
                    { cancelable: false },
                );
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }

    async onSelection(id) {
        await this.setState({ selectedFollowerId: id });
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                {this.state.result &&
                    <FlatList
                        data={this.state.result}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.onSelection(item.id)} style={styles.flatlistContainer}>
                                <View>
                                    <Image source={images.Offermage} resizeMode='cover' style={styles.restaurantImageStyle} />
                                </View>
                                <View style={{ flex: 0.80, marginLeft: Metrics.CountScale(10), justifyContent: 'center' }}>
                                    <Text style={{ fontSize: Metrics.CountScale(16) }} numberOfLines={1}>
                                        {item.firstName} {item.lastName}
                                    </Text>
                                </View>
                                <View style={styles.followButtonViewStyle}>
                                    {this.state.selectedFollowerId == item.id && <Icon name="check" size={24} color={Colors.RECOMMENDEDBUTTON} />}
                                </View>
                            </TouchableOpacity>
                        )}
                        extraData={this.state}
                        showsVerticalScrollIndicator={false}
                    />
                }
                <TouchableOpacity onPress={() => this.recommendRestaurant()} style={{ backgroundColor: Colors.NAVIGATIONLABEL, justifyContent: 'center', alignItems: 'center', height: Metrics.CountScale(50), width: '100%' }}>
                    <Text style={{ fontSize: Metrics.CountScale(16), color: Colors.WHITE }}>
                        SEND
                        </Text>
                </TouchableOpacity>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </SafeAreaView >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(20),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    flatlistContainer: {
        paddingHorizontal: Metrics.CountScale(22),
        flexDirection: 'row',
        flex: 1,
        paddingVertical: Metrics.CountScale(18)
    },
    restaurantImageStyle: {
        borderRadius: Metrics.CountScale(42) / 2,
        width: Metrics.CountScale(42),
        height: Metrics.CountScale(42)
    },
    followButtonViewStyle: {
        width: Metrics.CountScale(20),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.20,
        alignSelf: 'center'
    },
    followButtonTextStyle: {
        fontSize: Metrics.CountScale(14),
        color: Colors.WHITE
    }
});

export default RecommendFollowerList;
