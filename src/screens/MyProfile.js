import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, FlatList, AsyncStorage } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import ProfileView from '../components/common/ProfileView';
import { get } from 'lodash';
import { Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

const items = [
    {
        name: 'Hariom Raje'
    }
];

let access_Token;
let body = '';
let header = '';
let token = '';

class MyProfile extends React.Component {

    state = {
        result: [],
        loading: false
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.getUserProfile();
        EventRegister.addEventListener('UpdateProfile', (response) => {
            this.getUserProfile();
        });
    }

    getUserProfile() {

        this.setState({ loading: true });

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/56/profile-dashboard', 'get', token, body, header).then(async (json) => {
            this.setState({ loading: false });
            await this.setState({ result: [json] });
            console.log(this.state.result);
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    following() {
        this.props.navigation.navigate('Following');
    }
    followers() {
        this.props.navigation.navigate('Followers');
    }
    visited() {
        this.props.navigation.navigate('Visited');
    }
    top10() {
        this.props.navigation.navigate('Top10');
    }
    myAsk() {
        alert('MyAsk Screen')
    }
    recommended() {
        this.props.navigation.navigate('Recommended');
    }
    recommendedForMe() {
        this.props.navigation.navigate('RecommendedForMe');
    }
    wantToGo() {
        this.props.navigation.navigate('WantToGo');
    }
    reviewed() {
        this.props.navigation.navigate('Reviewed');
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView bounces={false}>
                    <FlatList
                        extraData={this.state}
                        data={this.state.result}
                        renderItem={({ item }) => <ProfileView data={item} flag={false} navigate={get(this.props, 'navigation', null)} />}
                    />
                    <View style={{ paddingVertical: Metrics.CountScale(15) }}>
                        <View style={styles.commonViewStyle}>
                            <TouchableOpacity onPress={() => this.top10()} style={[styles.commonStyle, { backgroundColor: Colors.TOP10 }]}>
                                <Text numberOfLines={2} style={{ fontSize: Metrics.CountScale(20), fontWeight: 'bold', color: Colors.WHITE, padding: Metrics.CountScale(15) }}>
                                    TOP 10 Restaurants
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.myAsk()} style={[styles.commonStyle, { backgroundColor: Colors.MYASK }]}>
                                <View style={{ padding: Metrics.CountScale(15) }}>
                                    <Text numberOfLines={1} style={[styles.commonNumberTextStyle, { color: Colors.BLACK }]}>
                                        {this.state.result.length != 0 &&
                                            this.state.result[0].asked
                                        }
                                    </Text>
                                    <Text numberOfLines={1} style={[styles.commonTextStyle, { color: Colors.BLACK }]}>
                                        My Asks
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.commonViewStyle}>
                            <TouchableOpacity onPress={() => this.recommended()} style={[styles.commonStyle, { backgroundColor: Colors.RECOMMENDED }]}>
                                <View style={{ padding: Metrics.CountScale(15) }}>
                                    <Text numberOfLines={1} style={[styles.commonNumberTextStyle, { color: Colors.WHITE }]}>
                                        {this.state.result.length != 0 &&
                                            this.state.result[0].recommended
                                        }
                                    </Text>
                                    <Text numberOfLines={2} style={[styles.commonTextStyle, { color: Colors.WHITE }]}>
                                        Recommended
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.recommendedForMe()} style={[styles.commonStyle, { backgroundColor: Colors.RECOMMENDEDFORME }]}>
                                <View style={{ padding: Metrics.CountScale(15) }}>
                                    <Text numberOfLines={1} style={[styles.commonNumberTextStyle, { color: Colors.WHITE }]}>
                                        {this.state.result.length != 0 &&
                                            this.state.result[0].recommendedToMe
                                        }
                                    </Text>
                                    <Text numberOfLines={2} style={[styles.commonTextStyle, { color: Colors.WHITE }]}>
                                        Recommended for Me
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.commonViewStyle}>
                            <TouchableOpacity onPress={() => this.wantToGo()} style={[styles.commonStyle, { backgroundColor: Colors.NAVIGATIONLABEL }]}>
                                <View style={{ padding: Metrics.CountScale(15) }}>
                                    <Text numberOfLines={1} style={[styles.commonNumberTextStyle, { color: Colors.WHITE }]}>
                                        {this.state.result.length != 0 &&
                                            this.state.result[0].wishList
                                        }
                                    </Text>
                                    <Text numberOfLines={2} style={[styles.commonTextStyle, { color: Colors.WHITE }]}>
                                        I Want to Go
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.reviewed()} style={[styles.commonStyle, { backgroundColor: Colors.REVIEWED }]}>
                                <View style={{ padding: Metrics.CountScale(15) }}>
                                    <Text numberOfLines={1} style={[styles.commonNumberTextStyle, { color: Colors.WHITE }]}>
                                        {this.state.result.length != 0 &&
                                            this.state.result[0].reviewed
                                        }
                                    </Text>
                                    <Text numberOfLines={2} style={[styles.commonTextStyle, { color: Colors.WHITE }]}>
                                        Reviewed
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    commonStyle: {
        width: Metrics.screenWidth / 2.4,
        height: ((Metrics.screenWidth / 2.4) / 0.82),
        borderRadius: Metrics.CountScale(10),
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    commonViewStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Metrics.CountScale(25),
        paddingVertical: Metrics.CountScale(5)
    },
    commonNumberTextStyle: {
        fontSize: Metrics.CountScale(32),
        fontWeight: 'bold',
        paddingBottom: Metrics.CountScale(20)
    },
    commonTextStyle: {
        fontSize: Metrics.CountScale(17),
        height: Metrics.CountScale(43)
    }
});

export default MyProfile;