import React from 'react';
import { Text, View, StyleSheet, ScrollView, FlatList, AsyncStorage } from 'react-native';
import ProfileView from '../components/common/ProfileView';
import { get } from 'lodash';
import OtherUserProfileView from '../components/common/OtherUserProfileView';
import { Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

const user = [
    {
        name: 'Hariom Raje'
    }
];

const items = [
    {
        name: 'Advertising Secrets'
    },
    {
        name: 'Be Single Minded'
    },
    {
        name: 'Advertising Secrets'
    }
];

let access_Token;
let body = '';
let header = '';
let token = '';
let endPoint = '';

class Profile extends React.Component {

    state = {
        result: [],
        top10: [],
        ask: [],
        recommended: [],
        wantToGo: [],
        reviews: [],
        loading: false
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.setState({ loading: true });

        this.getUserProfile();
        this.getTop10Restaurant();
        this.getRecommended();
        this.getWantToGo();
        this.getReviews();

        setTimeout(() => {
            this.setState({ loading: false });
        }, 1500);
    }

    getRecommended() {

        endPoint = `users/api/v1/users/${this.props.navigation.state.params.userId}/reccomendedtome-restautants`;
        token = 'Bearer ' + access_Token;

        APICaller(endPoint, 'get', token, body, header).then(async (json) => {
            console.log(json);
            await this.setState({ recommended: json });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    getWantToGo() {

        endPoint = `users/api/v1/users/${this.props.navigation.state.params.userId}/wishlisted-restautants`;
        token = 'Bearer ' + access_Token;

        APICaller(endPoint, 'get', token, body, header).then(async (json) => {
            console.log(json);
            await this.setState({ wantToGo: json });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    getReviews() {

        endPoint = `users/api/v1/users/${this.props.navigation.state.params.userId}/reviewed-restautants`;
        token = 'Bearer ' + access_Token;

        APICaller(endPoint, 'get', token, body, header).then(async (json) => {
            console.log(json);
            await this.setState({ reviews: json });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    getTop10Restaurant() {

        endPoint = `users/api/v1/users/${this.props.navigation.state.params.userId}/top-restaurants`;
        token = 'Bearer ' + access_Token;

        APICaller(endPoint, 'get', token, body, header).then(async (json) => {
            console.log(json);
            await this.setState({ top10: json });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    getUserProfile() {

        console.log(this.props.navigation.state.params.userId);

        endPoint = `users/api/v1/users/${this.props.navigation.state.params.userId}/profile-dashboard`;
        token = 'Bearer ' + access_Token;

        APICaller(endPoint, 'get', token, body, header).then(async (json) => {
            await this.setState({ result: [json] });
            console.log(this.state.result);
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView bounces={false}>
                    <FlatList
                        extraData={this.state}
                        data={this.state.result}
                        renderItem={({ item }) => <ProfileView data={item} flag={true} navigate={get(this.props, 'navigation', null)} />}
                    />
                    <View style={styles.flatlistView}>
                        <Text numberOfLines={1} style={styles.restaurantPlaceText}>TOP 10 Restaurants</Text>
                        <FlatList
                            data={this.state.top10}
                            horizontal={true}
                            renderItem={({ item, index }) => <OtherUserProfileView data={item} padd={15} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={styles.flatlistView}>
                        <Text numberOfLines={1} style={styles.restaurantPlaceText}>
                            Asks
                            {this.state.result.length != 0 &&
                                ' (' + this.state.result[0].asked + ')'
                            }
                        </Text>
                        <FlatList
                            data={this.state.ask}
                            horizontal={true}
                            renderItem={({ item, index }) => <OtherUserProfileView data={item} padd={15} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={styles.flatlistView}>
                        <Text numberOfLines={1} style={styles.restaurantPlaceText}>
                            Recommended
                            {this.state.result.length != 0 &&
                                ' (' + this.state.result[0].recommended + ')'
                            }
                        </Text>
                        <FlatList
                            data={this.state.recommended}
                            horizontal={true}
                            renderItem={({ item, index }) => <OtherUserProfileView data={item} padd={15} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={styles.flatlistView}>
                        <Text numberOfLines={1} style={styles.restaurantPlaceText}>
                            I Want to Go
                            {this.state.result.length != 0 &&
                                ' (' + this.state.result[0].wishList + ')'
                            }
                        </Text>
                        <FlatList
                            data={this.state.wantToGo}
                            horizontal={true}
                            renderItem={({ item, index }) => <OtherUserProfileView data={item} padd={15} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={styles.flatlistView}>
                        <Text numberOfLines={1} style={styles.restaurantPlaceText}>
                            Reviews
                            {this.state.result.length != 0 &&
                                ' (' + this.state.result[0].reviewed + ')'
                            }
                        </Text>
                        <FlatList
                            data={this.state.reviews}
                            horizontal={true}
                            renderItem={({ item, index }) => <OtherUserProfileView data={item} padd={15} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                </ScrollView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    flatlistView: {
        flex: 1,
        paddingTop: Metrics.CountScale(15),
        paddingBottom: Metrics.CountScale(15)
    },
    restaurantPlaceText: {
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(15),
        fontWeight: 'bold',
        paddingLeft: Metrics.CountScale(20)
    }
});

export default Profile;