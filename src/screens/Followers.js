import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, TextInput, FlatList, AsyncStorage } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import { BlackBackIcon } from '../components/common/Header';
import FollowListView from '../components/common/FollowListView';
import { images, Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let self, access_Token;
let body = '';
let header = '';
let token = '';

const items = [
    {
        name: 'Ashley Cooper',
        description: 'Friends on Facebook',
        following: true
    },
    {
        name: 'Cecelia Rodriquez',
        description: 'Friends on Facebook',
        following: true
    },
    {
        name: 'Lillian Morrison',
        description: 'Friends on Facebook',
        following: false
    },
    {
        name: 'Amelia Chavez',
        description: 'Friends on Facebook',
        following: true
    },
    {
        name: 'GeneVieve Munoz',
        description: 'Friends on Facebook',
        following: false
    },
    {
        name: 'Helena Lewis',
        description: 'Friends on Facebook',
        following: false
    },
    {
        name: 'Marion Mendez',
        description: 'Friends on Facebook',
        following: true
    },
    {
        name: 'Kathryn Ryan',
        description: 'Friends on Facebook',
        following: true
    },
];

class Followers extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Followers',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.FOLLOWING, borderBottomWidth: 0, elevation: 0 },
        headerLeft: <TouchableOpacity onPress={() => self.back()}>
            <BlackBackIcon />
        </TouchableOpacity>,
        headerRight: <View></View>
    });

    state = {
        result: [],
        loading: false
    }

    async componentDidMount() {
        self = this;

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.getFollowerList();
        EventRegister.addEventListener('UpdateFollowerList', (response) => {
            this.getFollowerList();
        });
    }

    getFollowerList() {

        token = 'Bearer ' + access_Token;
        // const endPoint = `users/${56}/followers`;

        this.setState({ loading: true });

        APICaller('users/api/v1/users/56/followers', 'get', token, body, header).then(async (json) => {
            console.log(json);
            this.setState({ result: json, loading: false })
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    back() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.searchBackgroundView}>
                    <View style={styles.searchInputStyle}>
                        <View style={{ justifyContent: 'center', paddingLeft: Metrics.CountScale(12) }}>
                            <Image source={images.SearchIcon} resizeMode='center' />
                        </View>
                        <View style={styles.textinputViewStyle}>
                            <TextInput
                                placeholder="Foods, restaurants, friends.."
                                placeholderTextColor={Colors.GRAY}
                                selectionColor={Colors.BLACK}
                                style={styles.textinputStyle}
                            />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        extraData={this.state}
                        data={this.state.result}
                        renderItem={({ item, index }) => <FollowListView data={item} index={index} />}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.BLACK,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center'
    },
    textinputViewStyle: {
        flex: 1,
        padding: Metrics.CountScale(15),
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        paddingLeft: Metrics.CountScale(12)
    },
    textinputStyle: {
        paddingTop: Metrics.CountScale(0),
        paddingBottom: Metrics.CountScale(0),
        fontSize: Metrics.CountScale(15)
    },
    searchBackgroundView: {
        backgroundColor: Colors.FOLLOWING,
        paddingBottom: Metrics.CountScale(15),
        padding: Metrics.CountScale(15),
        paddingTop: Metrics.CountScale(0)
    },
    searchInputStyle: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        padding: Metrics.CountScale(5)
    }
});

export default Followers;