import React from 'react';
import { Text, View, StyleSheet, KeyboardAvoidingView, TouchableOpacity, Platform, SafeAreaView, Dimensions, Alert } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextField } from 'react-native-material-textfield';
import { Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

const { height, width } = Dimensions.get('window');
const isX = !!(height === 812 || width === 812);

let header = '';
let token = '';

class ForgotPassword extends React.Component {

    state = {
        email_id: '',
        loading: false
    };

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'FORGOT PASSWORD',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.WHITE },
        headerTintColor: Colors.ORANGE,
        headerRight: <View></View>
    });

    back() {
        this.props.navigation.goBack();
    }

    forgotPassword() {

        if (this.state.email_id.trim() == '') {
            alert('Please enter email.')
        }
        else if (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email_id) == false) {
            alert('Please enter valid email.')
        }
        else {
            this.setState({ loading: true });

            var formData = new FormData();

            formData.append("email", this.state.email_id);

            APICaller('users/api/v1/users/forget-password-mail', 'post', token, formData, header).then(async (json) => {
                console.log(json);
                this.setState({ loading: false });

                if (json.status == 'success') {
                    Alert.alert(
                        '',
                        'Password has been sent to given email address.',
                        [
                            { text: 'Ok', onPress: () => { this.back() } },
                        ],
                        { cancelable: false },
                    );
                }
                else {
                    alert(json.message);
                }
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }

    offset() {
        if (isX) {
            return 88;
        }
        return 64;
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <KeyboardAvoidingView
                    style={{ flex: 1 }}
                    behavior={Platform.OS === 'ios' ? "padding" : null}
                    keyboardVerticalOffset={Platform.OS === 'ios' ? this.offset() : 80}
                    enabled
                >
                    <KeyboardAwareScrollView
                        style={{ flex: 1 }}
                        enableAutomaticScroll={false}
                        contentContainerStyle={{ flex: 1 }}
                        keyboardShouldPersistTaps={'handled'}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={{ marginTop: Metrics.CountScale(20) }}>
                            <Text style={styles.txtStyle}>
                                Enter your email address below {"\n"} and we`all send you a secure link to reset {"\n"} your password.
                            </Text>
                        </View>

                        <View style={styles.viewEmail}>
                            <TextField
                                label='EMAIL'
                                textColor={Colors.BLACK}
                                tintColor={Colors.GRAY}
                                fontSize={Metrics.CountScale(15)}
                                value={this.state.email_id}
                                onChangeText={val => this.setState({ email_id: val })}
                            />
                        </View>
                    </KeyboardAwareScrollView>
                    <View style={styles.viewResetButton}>
                        <TouchableOpacity onPress={() => { this.forgotPassword() }} >
                            <View style={{ alignItems: 'center', backgroundColor: Colors.ORANGE, height: Metrics.CountScale(50), width: '100%', justifyContent: 'center' }}>
                                <Text style={styles.forgotPasswordText}>Reset Password</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    forgotPasswordText: {
        color: Colors.WHITE,
        fontSize: Metrics.CountScale(18),
    },
    txtStyle: {
        textAlign: 'center',
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(16),
    },
    viewEmail: {
        flex: 1,
        justifyContent: 'center',
        padding: Metrics.CountScale(25)
    },
    viewResetButton: {
        justifyContent: 'flex-end'
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(20),
        color: Colors.ORANGE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
});

export default ForgotPassword;