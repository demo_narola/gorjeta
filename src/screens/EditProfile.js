import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TouchableWithoutFeedback, Image, Modal, ImageBackground, AsyncStorage } from 'react-native';
import { Metrics, Colors, images } from '../assets/index';
import ImagePicker from 'react-native-image-crop-picker';
import { TextField } from 'react-native-material-textfield';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import APICaller from '../api/Api';

let access_Token;
let header = '';
let token = '';

class EditProfile extends Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Edit Profile',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    async componentDidMount() {
        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    state = {
        image: '',
        firstname: '',
        lastname: '',
        updateProfileImage: '',
        modalVisible: false,
    }

    setModalVisible() {
        this.setState({ modalVisible: true });
    }

    async pickFromGallery() {
        await this.setState({ modalVisible: false });
        ImagePicker.openPicker({
            mediaType: 'photo',
            width: 400,
            height: 400,
            includeBase64: true,
            compressImageQuality: 0.5
        }).then(image => {
            imageData = { uri: image.path, type: image.mime, name: image.filename == null ? 'IMG1.jpg' : image.filename, data: image.data }
            this.setState({ image: imageData, updateProfileImage: imageData.data });
        }).catch(e => {
            console.log(e);
        });
    }

    async pickFromcamera() {
        await this.setState({ modalVisible: false });
        ImagePicker.openCamera({
            mediaType: 'photo',
            width: 400,
            height: 400,
            includeBase64: true,
            compressImageQuality: 0.5,
        }).then(image => {
            imageData = { uri: image.path, type: image.mime, name: image.filename == null ? 'IMG1.jpg' : image.filename, data: image.data }
            this.setState({ image: imageData, updateProfileImage: imageData.data });
        }).catch(e => {
            console.log(e);
        });
    }

    async openGallery() {
        await this.setState({ modalVisible: false });
        setTimeout(() => { this.pickFromGallery() }, 500)
    }

    async openCamera() {
        await this.setState({ modalVisible: false });
        setTimeout(() => { this.pickFromcamera() }, 500)
    }

    updateUserDetail() {

        if (this.state.firstname.trim() == '') {
            alert('Please enter firstname.')
        } else if (this.state.lastname.trim() == '') {
            alert('Please enter lastname.')
        } else {
            this.setState({ loading: true });

            var formData = new FormData();

            formData.append('firstName', this.state.firstname);
            formData.append('lastName', this.state.lastname);
            formData.append('image', this.state.updateProfileImage);

            token = 'Bearer ' + access_Token;

            APICaller('users/api/v1/users/updateprofile', 'put', token, formData, header).then(async (json) => {
                console.log(json);
                await this.setState({ loading: false });
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }

    }


    render() {
        return (
            <View style={{ flex: 1 }} >
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ padding: Metrics.CountScale(25) }}>
                        <TouchableOpacity onPress={() => this.setModalVisible()} style={styles.profileViewImageSize}>
                            <ImageBackground source={images.DummyProfileImage} style={styles.profileImageSize}>
                                <Image source={this.state.image} style={styles.profileImageSize} />
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: Metrics.CountScale(25) }}>
                        <TextField
                            label='First Name'
                            textColor={Colors.GRAY}
                            fontSize={Metrics.CountScale(18)}
                            tintColor={Colors.NAVIGATIONLABEL}
                            value={this.state.firstname}
                            onChangeText={val => this.setState({ firstname: val })}
                            autoFocus={false}
                        />
                        <TextField
                            label='Last Name'
                            textColor={Colors.GRAY}
                            fontSize={Metrics.CountScale(18)}
                            tintColor={Colors.NAVIGATIONLABEL}
                            value={this.state.lastname}
                            onChangeText={val => this.setState({ lastname: val })}
                            autoFocus={false}
                        />
                        <TouchableOpacity onPress={() => this.updateUserDetail()} style={styles.saveButtonStyle}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }} >
                                <Text style={styles.saveTextStyle}>Save</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                <View>
                    <Modal
                        animationType="fade"
                        transparent={true}
                        visible={this.state.modalVisible}
                    >
                        <TouchableWithoutFeedback onPress={() => { this.setState({ modalVisible: false }) }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0, 0, 0, 0.5)' }}>
                                <TouchableOpacity onPress={() => { }}>
                                    <View style={{ height: Metrics.CountScale(250), width: Metrics.CountScale(330), backgroundColor: Colors.WHITE, borderRadius: Metrics.CountScale(5), justifyContent: 'space-around' }}>
                                        <View style={{ alignItems: 'center' }}>
                                            <Text style={styles.modalTextStyle}>CHOOSE IMAGE</Text>
                                        </View>
                                        <View style={{ justifyContent: 'space-around', flexDirection: 'row' }}>
                                            <TouchableOpacity onPress={() => this.openGallery()}>
                                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={images.GalleryIcon} resizeMode='stretch' style={{ height: Metrics.CountScale(105), width: Metrics.CountScale(105) }} />
                                                    <Text style={styles.commonTextStyle}>GALLERY</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={() => this.openCamera()} >
                                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={images.BigCameraIcon} resizeMode='stretch' style={{ height: Metrics.CountScale(105), width: Metrics.CountScale(105) }} />
                                                    <Text style={styles.commonTextStyle}>CAMERA</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </TouchableWithoutFeedback>
                    </Modal>
                </View>
            </View >
        )
    }
}

const styles = {
    saveTextStyle: {
        color: Colors.WHITE,
        fontSize: Metrics.CountScale(20)
    },
    saveButtonStyle: {
        width: '100%',
        height: Metrics.CountScale(45),
        backgroundColor: Colors.NAVIGATIONLABEL,
        borderRadius: Metrics.CountScale(25),
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: Metrics.CountScale(40)
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    profileImageSize: {
        height: Metrics.CountScale(110),
        width: Metrics.CountScale(110),
        borderRadius: Metrics.CountScale(110 / 2)
    },
    profileViewImageSize: {
        height: Metrics.CountScale(110),
        width: Metrics.CountScale(110),
        borderRadius: Metrics.CountScale(110 / 2),
        backgroundColor: '#e2e2e2',
        alignItems: 'center',
        alignSelf: 'center'
    },
    commonTextStyle: {
        marginTop: Metrics.CountScale(10),
        color: Colors.GRAY,
        fontSize: Metrics.CountScale(16)
    }
}

export default EditProfile
