import React from 'react';
import { Text, View, StyleSheet, FlatList, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import _, { get } from 'lodash';
import { images, Metrics, Colors } from '../assets/index';
import RecommendUserList from '../components/common/RecommendUserList';
import FoodFeedview from '../components/common/FoodFeedview';
import { EventRegister } from 'react-native-event-listeners';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let access_Token;
let body = '';
let header = '';
let token = '';

const items = [
    {
        firstName: 'Hariom',
        lastName: 'Raje'
    },
    {
        firstName: 'Rohan',
        lastName: 'Mali'
    },
    {
        firstName: 'Nitin',
        lastName: 'Gadhiya'
    },
    {
        firstName: 'Monica',
        lastName: 'Vekariya',
    }
];

class FoodFeed extends React.Component {

    state = {
        result: [],
        loading: false
    }

    async componentDidMount() {
        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        this.getFoodFeedDetail();

        EventRegister.addEventListener('UpdateFoodFeedData', async (response) => {
            console.log(response)
            await this.setState({ loading: true })
            const index = _.findIndex(this.state.result, { restaurantId: response });
            console.log(index)
            if (index > -1) {
                console.log(index)
                if (this.state.result[index].wishListed == false) {
                    console.log(this.state.result[index].wishListed, this.state.result[index].wishListCount, '1111')
                    this.state.result[index].wishListCount = (this.state.result[index].wishListCount) + 1;
                    this.state.result[index].wishListed = true;
                }
                else {
                    console.log(this.state.result[index].wishListed, this.state.result[index].wishListCount, '2222')
                    this.state.result[index].wishListCount = (this.state.result[index].wishListCount) - 1;
                    this.state.result[index].wishListed = false;
                }
            }
            await this.setState({ result: this.state.result, loading: false });
        });
    }

    getFoodFeedDetail() {

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/foodfeed', 'get', token, body, header).then(async (json) => {
            await this.setState({ result: json });
            console.log(json);
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });

    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'FoodFeed',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.WHITE },
    });

    renderGroupPost = () => {
        return (
            <View style={styles.viewStyle}>
                <TouchableOpacity onPress={() => this.askForRecommendation()}>
                    <Image source={images.AskRecommendationLogo} resizeMode='cover' style={{ height: Metrics.CountScale(65), width: Metrics.CountScale(65) }} />
                </TouchableOpacity>
                <View style={{ marginTop: Metrics.CountScale(10) }}>
                    <Text numberOfLines={1} style={styles.textStyle}>Ask my friend</Text>
                </View>
            </View>
        );
    };

    askForRecommendation() {
        this.props.navigation.navigate('AskForRecommendation');
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={{ paddingVertical: Metrics.CountScale(10) }}>
                        <FlatList
                            data={items}
                            horizontal={true}
                            renderItem={({ item, index }) => <RecommendUserList data={item} index={index} navigate={get(this.props, 'navigation', null)} />}
                            ListHeaderComponent={this.renderGroupPost}
                            showsHorizontalScrollIndicator={false}
                        />
                    </View>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            extraData={this.state}
                            data={this.state.result}
                            renderItem={({ item }) => <FoodFeedview data={item} navigate={get(this.props, 'navigation', null)} />}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                </ScrollView>
                <TouchableOpacity style={{ position: 'absolute', bottom: 5, right: 0 }}>
                    <Image source={images.EditPenIcon} resizeMode='cover' />
                </TouchableOpacity>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.BLACK,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center'
    },
    viewStyle: {
        marginVertical: Metrics.CountScale(10),
        alignItems: 'center',
        marginLeft: Metrics.CountScale(16)
    },
    textStyle: {
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(12),
        width: Metrics.CountScale(90),
        textAlign: 'center'
    }
});

export default FoodFeed;