import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, TextInput, FlatList, AsyncStorage } from 'react-native';
import { BlackBackIcon } from '../components/common/Header';
import { get } from 'lodash';
import RestaurantListView from '../components/common/RestaurantListView';
import { images, Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let self, access_Token;
let body = '';
let header = '';
let token = '';

const items = [
    {
        name: 'Effective Advertisement',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
    {
        name: 'Henry Tran',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
    {
        name: 'Henry Tran',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
    {
        name: 'Henry Tran',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
    {
        name: 'Henry Tran',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
    {
        name: 'Henry Tran',
        description: 'Internet Banner Advertising Most Reliable Form Advertising'
    },
];

class Visited extends React.Component {

    state = {
        result: [],
        loading: false
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'I Visited',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.VISITED, borderBottomWidth: 0, elevation: 0 },
        headerLeft: <TouchableOpacity onPress={() => self.back()}>
            <BlackBackIcon />
        </TouchableOpacity>,
        headerRight: <View></View>
    });

    async componentDidMount() {

        self = this;

        this.setState({ loading: true });

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/88/visited-restautants', 'get', token, body, header).then(async (json) => {
            console.log(json);
            await this.setState({ result: json, loading: false });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    back() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.searchBackgroundView}>
                    <View style={styles.searchInputStyle}>
                        <View style={{ justifyContent: 'center', paddingLeft: Metrics.CountScale(12) }}>
                            <Image source={images.SearchIcon} resizeMode='center' />
                        </View>
                        <View style={styles.textinputViewStyle}>
                            <TextInput
                                placeholder="Foods, restaurants, friends.."
                                placeholderTextColor={Colors.GRAY}
                                selectionColor={Colors.BLACK}
                                style={styles.textinputStyle}
                            />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        data={this.state.result}
                        renderItem={({ item }) => <RestaurantListView data={item} screenName={'Visited'} />}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(17),
        color: Colors.BLACK,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center'
    },
    textinputViewStyle: {
        flex: 1,
        padding: Metrics.CountScale(15),
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        paddingLeft: Metrics.CountScale(12)
    },
    textinputStyle: {
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: Metrics.CountScale(15)
    },
    searchBackgroundView: {
        backgroundColor: Colors.VISITED,
        paddingBottom: Metrics.CountScale(15),
        padding: Metrics.CountScale(15),
        paddingTop: Metrics.CountScale(0)
    },
    searchInputStyle: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        padding: Metrics.CountScale(5)
    }
});

export default Visited;