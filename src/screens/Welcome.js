import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ImageBackground, Image, Platform, WebView } from 'react-native';
import Swiper from 'react-native-swiper';
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import SplashScreen from 'react-native-splash-screen';
import { Crashlytics } from 'react-native-fabric';
import { images, Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

const header = '';

const slide = [
    {
        image: images.BackgroundImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.BackgroundImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.BackgroundImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
    {
        image: images.BackgroundImage,
        text: 'Nailing It On The Head With Free Internet Advertising'
    },
]

class Welcome extends React.Component {

    state = {
        loading: false
    };

    login() {
        this.props.navigation.navigate('Login');
    }
    register() {
        this.props.navigation.navigate('Registration');
    }
    facebookLogin() {
        this.props.navigation.navigate('TabBar');
    }
    // gmailLogin() {
    //     this.props.navigation.navigate('TabBar');
    // }

    componentDidMount() {
        setTimeout(() => { SplashScreen.hide() }, 1000);
        // setTimeout(() => { Crashlytics.crash() }, 10000);

        // var formData = new FormData();

        // formData.append('username', 'josh@yopmail.com');
        // formData.append('password', 'test@123');
        // formData.append('grant_type', 'password');

        // fetch('http://192.168.1.243:8765/auth/login/facebook', {
        //     method: 'post',
        //     // headers: {
        //     //     'Content-Type': 'application/x-www-form-urlencoded'
        //     // },
        // })
        //     .then((response) => {
        //         console.log(response, 'Nothing');

        //         // if (response) {
        //         //     return response.json();
        //         // } else {
        //         //     alert('Nothing');
        //         //     return false;
        //         // }
        //     })
        //     .then((responseJson) => {
        //         // console.log(responseJson);
        //         // if (!responseJson || (responseJson && responseJson.status && responseJson.status != 200)) {
        //         //     alert(responseJson.message);
        //         //     console.log(responseJson);
        //         //     return;
        //         // }
        //         // console.log(responseJson);
        //     })

        // const body = '';
        // const header = '';

        // APICaller('users/95/recommendedbyme-restautants', 'get', body, header).then(async (json) => {
        //     console.log(json, '*************');
        // })
        //     .catch((error) => {
        //         alert('Something went wrong, try again.')
        //         console.error(error);
        //     });

        // GoogleSignin.configure({
        //     webClientId: '763559903735-cjppv7vp93oblv54e41ikjcharlub8pk.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
        //     offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        // });

        // const data = {
        //     "firstName": "abc",
        //     "lastName": "xyz",
        //     "email": "abc@yopmail.com",
        //     "mobileNo": "000000000",
        //     "avtar": ""
        // };
        // const body = JSON.stringify(data);
        // const header = ''


        // APICaller('users', 'post', body, header).then(async (json) => {
        //     console.log(json, '*************');
        // })
        //     .catch((error) => {
        //         alert('Something went wrong, try again.')
        //         console.error(error);
        //     });

        //http://192.168.1.243:8090/api/v1/users/95/recommendedbyme-restautants

        GoogleSignin.configure({
            webClientId: '763559903735-cjppv7vp93oblv54e41ikjcharlub8pk.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
        });
    }

    gmailLogin = async () => {
        try {
            // await GoogleSignin.hasPlayServices();

            try {
                await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
            } catch (err) {
                console.error('play services are not available');
            }

            const info = await GoogleSignin.signIn();
            console.log(info);
        } catch (error) {
            console.log(error);
        }

        // try {
        //     await GoogleSignin.revokeAccess();
        //     await GoogleSignin.signOut();
        // } catch (error) {
        //     console.error(error);
        // }
    }

    auth() {
        LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
            function (result) {
                if (result.isCancelled) {
                    console.log('Login cancelled')
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            console.log(data.accessToken.toString())
                            fetch('https://graph.facebook.com/v2.5/me?fields=email,id,name,picture.type(large)&access_token=' + data.accessToken.toString())
                                .then((response) => response.json())
                                .then((json) => {
                                    console.log(json);
                                })
                                .catch(() => {
                                    alert('ERROR GETTING DATA FROM FACEBOOK')
                                })

                        })
                    console.log('Login success with permissions: ' + result.grantedPermissions.toString())
                }
            },
            function (error) {
                console.log('Login fail with error: ' + error)
            }
        )
        // this.setState({ loading: true });

        // const body = '';

        // APICaller('auth/login/facebook', 'post', body, header).then(async (json) => {
        //     console.log(json);
        //     this.setState({ loading: false });
        // })
        //     .catch((error) => {
        //         this.setState({ loading: false });
        //         alert('Something went wrong, try again.')
        //         console.error(error);
        //     });
    }

    render() {
        return (
            <View style={styles.container}>
                {/* <ImageBackground source={images.BackgroundImage} style={{ flex: 1 }} resizeMode='cover' >
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <View>
                            <Image source={images.AppLogo} resizeMode={'cover'} />
                        </View>

                        <TouchableOpacity onPress={() => this.login()}>
                            <Text>
                                Login
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.registration()}>
                            <Text>
                                Registration
                    </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.facebookLogin()}>
                            <Text>
                                Home
                    </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.gmailLogin()}>
                            <Text>
                                Home
                    </Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground> */}
                <Swiper
                    showsButtons={false}
                    autoplay={true}
                    dot={<Image source={images.DotIcon} resizeMode={'contain'} style={styles.dotStyle} />}
                    activeDot={<Image source={images.ActiveSlideIcon} resizeMode={'contain'} style={styles.dotStyle} />}
                >
                    {slide.map(inboxObj => (
                        <ImageBackground source={inboxObj.image} style={{ flex: 1 }} resizeMode={'cover'} >
                            <Text numberOfLines={2} style={styles.sliderTextStyle}>
                                {inboxObj.text}
                            </Text>
                        </ImageBackground>
                    ))}
                </Swiper>
                <View style={{ justifyContent: 'flex-end', width: '100%', position: 'absolute', paddingTop: Platform.OS == 'ios' ? (Metrics.screenHeight == 812 ? 44 : 20) : 0 }}>
                    <View style={{ alignItems: 'center', top: Metrics.screenHeight * 9 / 100, }}>
                        <Image source={images.AppLogo} resizeMode={'cover'} />
                    </View>
                </View>
                <View style={{ justifyContent: 'flex-end', width: '100%', position: 'absolute', bottom: Platform.OS == 'ios' ? (Metrics.screenHeight == 812 ? 50 : 10) : 20 }}>
                    <View style={styles.buttonSectionBottom}>
                        <TouchableOpacity onPress={() => this.auth()} style={[styles.buttonSectionBottomViewStyle, { marginRight: Metrics.CountScale(10) }]}>
                            <Image source={images.FbButton} resizeMode={'cover'} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.gmailLogin()} style={[styles.buttonSectionBottomViewStyle, { marginLeft: Metrics.CountScale(10) }]}>
                            <Image source={images.GmailButton} resizeMode={'cover'} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => this.login()} style={styles.signUpViewStyle}>
                        <Text style={{ color: Colors.WHITE, fontSize: Metrics.CountScale(15) }}>SIGN UP WITH EMAIL</Text>
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                        <Text style={{ color: Colors.WHITE, fontSize: Metrics.CountScale(15) }}>Don't have an account?</Text>
                        <TouchableOpacity onPress={() => this.register()} style={{ padding: Metrics.CountScale(10) }}>
                            <Text style={{ color: Colors.SIGNUP, fontWeight: 'bold' }}>SIGN UP</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    slider: {
        flex: 1
    },
    dotStyle: {
        margin: Metrics.CountScale(5),
        bottom: Metrics.screenHeight * 28 / 100
    },
    sliderTextStyle: {
        paddingHorizontal: Metrics.CountScale(32),
        textAlign: 'center',
        bottom: Metrics.screenHeight * 36 / 100,
        width: '100%',
        position: 'absolute',
        fontSize: Metrics.CountScale(22),
        color: Colors.WHITE
    },
    buttonSectionBottom: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Metrics.CountScale(32),
        paddingBottom: Metrics.CountScale(15)
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(45),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    signUpViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: Metrics.CountScale(32),
        height: Metrics.CountScale(50),
        borderWidth: 1,
        borderColor: Colors.WHITE,
        backgroundColor: 'transparent',
        borderRadius: Metrics.CountScale(30),
        marginBottom: Metrics.CountScale(15)
    }
});

export default Welcome;