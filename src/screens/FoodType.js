import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, Platform } from 'react-native';
import FoodTypeView from '../components/common/FoodTypeView';
import { images, Metrics, Colors } from '../assets/index';

const items = [
    {
        name: 'Healthy',
        flag: false,
        media: images.HealthyIcon
    },
    {
        name: 'Italian',
        flag: false,
        media: images.ItalianIcon
    },
    {
        name: 'Arabic',
        flag: false,
        media: images.ArabicIcon
    },
    {
        name: 'Japanese',
        flag: false,
        media: images.JapaneseIcon
    },
    {
        name: 'Chinese',
        flag: false,
        media: images.ChineseIcon
    },
    {
        name: 'Asian',
        flag: false,
        media: images.AsianIcon
    },
    {
        name: 'Burger',
        flag: false,
        media: images.BurgerIcon
    },
    {
        name: 'Sandwiches',
        flag: false,
        media: images.SandwichesIcon
    },
    {
        name: 'Brazilian',
        flag: false,
        media: images.BrazilianIcon
    },
    {
        name: 'Coffee',
        flag: false,
        media: images.CoffeeIcon
    },
    {
        name: 'Pizza',
        flag: false,
        media: images.PizzaIcon
    },
    {
        name: 'Korean',
        flag: false,
        media: images.KoreanIcon
    },
    {
        name: 'Bakery',
        flag: false,
        media: images.BakeryIcon
    },
    {
        name: 'Indian',
        flag: false,
        media: images.IndianIcon
    },
    {
        name: 'Barbecue',
        flag: false,
        media: images.BarbecueIcon
    },
    {
        name: 'SeaFood',
        flag: false,
        media: images.SeaFoodIcon
    },
    {
        name: 'Portuguese',
        flag: false,
        media: images.PortugueseIcon
    },
    {
        name: 'German',
        flag: false,
        media: images.GermanIcon
    }
];

class FoodType extends React.Component {

    state = {
        result: items
    }

    async setSelectSymbol(index) {
        if (this.state.result[index].flag == false) {
            this.state.result[index].flag = true;
        } else {
            this.state.result[index].flag = false;
        }
        let arr = this.state.result;
        await this.setState({ result: arr });
    }

    home() {
        this.props.navigation.navigate('TabBar');
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', padding: Metrics.CountScale(20), paddingBottom: Metrics.CountScale(0), alignItems: 'center' }}>
                    <View style={{ flex: 0.67, marginRight: Metrics.CountScale(2) }}>
                        <Text style={styles.foodTypeTextStyle}>
                            Tell your friends the food types you like.
                        </Text>
                    </View>
                    <View style={{ flex: 0.33 }}>
                        <TouchableOpacity onPress={() => { this.home() }} style={styles.nextButtonViewStyle}>
                            <Text style={styles.nextButtonTextStyle}>
                                Next
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ padding: Metrics.CountScale(20), paddingBottom: Metrics.CountScale(0), paddingTop: Metrics.CountScale(35), flex: 1 }}>
                    <FlatList
                        data={items}
                        contentContainerStyle={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            justifyContent: 'space-between'
                        }}
                        renderItem={({ item, index }) => <FoodTypeView data={item} setSelectSymbol={() => this.setSelectSymbol(index)} />}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        paddingTop: Platform.OS == 'ios' ? Metrics.CountScale(65) : Metrics.CountScale(0)
    },
    nextButtonTextStyle: {
        fontSize: Metrics.CountScale(17),
        color: Colors.WHITE
    },
    nextButtonViewStyle: {
        height: Metrics.CountScale(34),
        width: '100%',
        borderRadius: Metrics.CountScale(20),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#D82028'
    },
    foodTypeTextStyle: {
        fontSize: Metrics.CountScale(22),
        color: Colors.WHITE,
        fontWeight: '700'
    }
});

export default FoodType;