import React from 'react';
import { Text, View, StyleSheet, Image, ScrollView, ImageBackground, TouchableOpacity, Platform, FlatList, AsyncStorage } from 'react-native';
import { get } from 'lodash';
import ViewMoreText from 'react-native-view-more-text';
import StarRating from 'react-native-star-rating';
import MapView, { Marker } from 'react-native-maps';
import { EventRegister } from 'react-native-event-listeners';
import { images, Metrics, Colors } from '../assets/index';
import RecommendUserList from '../components/common/RecommendUserList';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

const starName = [
    {
        name: 'Food',
        star: 3
    },
    {
        name: 'Price',
        star: 2
    },
    {
        name: 'Environment',
        star: 5
    },
    {
        name: 'Waiter Service',
        star: 4
    }
];

const items = [
    {
        name: 'Hariom Raje',
    },
    {
        name: 'Rohan Mali',
    },
    {
        name: 'Nitin Ghadiya',
    },
    {
        name: 'Monica Vekariya',
    }
];

const tags = [
    {
        name: 'French',
    },
    {
        name: 'Breakfast & Brunch',
    },
    {
        name: 'Bakeries',
    },
    {
        name: 'Amazing',
    },
    {
        name: 'Delicious',
    },
    {
        name: 'Tasty',
    },
    {
        name: 'Hot',
    },
    {
        name: 'Dazzling',
    },
    {
        name: 'Perfect',
    },
    {
        name: 'Spicy',
    }
];
const color = ['#717CD8', '#53C3B4', '#FF545E', '#ffc0cb', '#b0e0e6', '#fa8072', '#468499', '#6897bb', '#003366', '#ffa500']

let i = 0;
let access_Token;
let body = '';
let header = '';
let token = '';

class RestaurantDetail extends React.Component {

    state = {
        displayMap: false,
        restaurantDetail: [],
        recommendedUser: [],
        averageEvaluation: [],
        myReview: [],
        loading: false
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });

        EventRegister.addEventListener('UpdateReview', (response) => {
            this.getAverageEvaluation();
            this.getMyRestaurantReview();
        });

        this.setState({ loading: true });

        this.getRestaurantDetail();
        this.getRecommendedUser();
        this.getAverageEvaluation();
        this.getMyRestaurantReview();

        setTimeout(() => {
            this.setState({ loading: false });
        }, 1500);


        setTimeout(() => {
            this.setState({ displayMap: true });
        }, 1500);
    }

    async getRestaurantDetail() {

        token = 'Bearer ' + access_Token;

        APICaller('restaurants/api/v1/restaurants/500', 'get', token, body, header).then(async (json) => {
            await this.setState({ restaurantDetail: json });
            console.log(json, '1');
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    async getRecommendedUser() {

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/56/followers?recommends=500', 'get', token, body, header).then(async (json) => {
            await this.setState({ recommendedUser: json });
            console.log(json, '2');
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    async getAverageEvaluation() {

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/restautant-reviews/500?evaluations=true', 'get', token, body, header).then(async (json) => {
            await this.setState({ averageEvaluation: json });
            console.log(json, '3');
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    async getMyRestaurantReview() {

        token = 'Bearer ' + access_Token;

        APICaller('users/api/v1/users/178/myreview-restaurant/500', 'get', token, body, header).then(async (json) => {
            console.log(json, '4');
            await this.setState({ myReview: json });
        })
            .catch((error) => {
                this.setState({ loading: false });
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    renderViewMore(onPress) {
        return (
            <Text style={{ color: Colors.BLACK, fontSize: Metrics.CountScale(15) }} onPress={onPress}>Read more</Text>
        )
    };
    renderViewLess(onPress) {
        return (
            <Text style={{ color: Colors.BLACK, fontSize: Metrics.CountScale(15) }} onPress={onPress}>Show less</Text>
        )
    };

    viewAllRating() {
        this.props.navigation.navigate('ViewAllRating');
    }

    followerList() {
        this.props.navigation.navigate('RecommendFollowerList');
    }

    back() {
        this.props.navigation.goBack();
    }

    giveReview() {
        this.props.navigation.navigate('MyReview');
    }

    async addAsVisited() {

        token = 'Bearer ' + access_Token;

        var formData = new FormData();

        formData.append("loginId", 56);
        formData.append("restaurantId", 2);

        APICaller('users/api/v1/users/visited-restaurant', 'post', token, formData, 'multipart/form-data', header).then(async (json) => {
            console.log(json, '5');
            await EventRegister.emit('UpdateProfile');
        })
            .catch((error) => {
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView bounces={false}>
                    <View style={{ flex: 1 }}>
                        <ImageBackground resizeMode={'cover'} source={{ uri: 'http://192.168.1.243:8765/restaurants/api/v1/restaurants/500/wallpaper' }} style={styles.imageBackgroundStyle}>
                            <View style={{ justifyContent: 'space-between', flex: 1 }}>
                                <View>
                                    <View style={styles.headerViewStyle}>
                                        <TouchableOpacity onPress={() => this.back()} style={styles.headerImageStyle} >
                                            <Image source={images.WhiteBackIcon} resizeMode={'cover'} />
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.headerImageStyle}>
                                            <Image source={images.RoundMoreIcon} resizeMode={'cover'} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View>
                                    <View style={styles.restaurantNameTextStyle}  >
                                        <Text numberOfLines={2} style={{ fontSize: Metrics.CountScale(25), color: Colors.WHITE }}>Jackson Market and Deli</Text>
                                    </View>
                                    <View style={styles.likeRestaurantView}>
                                        <View style={{ flexDirection: 'row', flex: 0.8 }}>
                                            <Image source={images.WhiteLocation} resizeMode={'contain'} style={{ marginRight: Metrics.CountScale(8) }} />
                                            <Text numberOfLines={1} style={{ fontSize: Metrics.CountScale(12), color: Colors.WHITE }}>4065, Jackson Avenue, culver city</Text>
                                        </View>
                                        <View style={styles.likeView}>
                                            <Image source={images.HeartLogo} resizeMode={'contain'} style={{ marginRight: Metrics.CountScale(13) }} />
                                            <Text numberOfLines={1} style={{ fontSize: Metrics.CountScale(14), color: Colors.WHITE }}>271</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={styles.recommendedUserSection}>
                        <Text style={styles.RecommendUserTextStyle}>Recommended By</Text>
                        <FlatList
                            data={this.state.recommendedUser}
                            horizontal={true}
                            renderItem={({ item, index }) => <RecommendUserList data={item} padd={20} index={index} navigate={get(this.props, 'navigation', null)} />}
                            showsHorizontalScrollIndicator={false}
                            contentContainerStyle={{ marginLeft: Metrics.CountScale(-5) }}
                        />
                    </View>

                    <View style={styles.tagSection}>
                        <View>
                            <FlatList
                                data={tags}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap'
                                }}
                                renderItem={({ item }) => {
                                    let a = i
                                    i++
                                    if (i == 10) {
                                        i = 0
                                    }
                                    return (
                                        <View style={[styles.restaurantTagView, { backgroundColor: color[a] }]}>
                                            <Text style={styles.restaurantTagTextStyle}>{item.name}</Text>
                                        </View>
                                    )
                                }}
                            />
                        </View>
                    </View>

                    <View style={styles.restaurantDescriptionSection}>
                        <ViewMoreText
                            numberOfLines={6}
                            renderViewMore={this.renderViewMore}
                            renderViewLess={this.renderViewLess}
                        >
                            <Text style={styles.restaurantDescriptionTextStyle}>
                                Wow I have a hankering for some really good grill roasted chicken, the melt in your mouth variety with some fresh homemade salsa slathered right on top.Just seems that we never have time during the lazy days of summer to get everything done. You know, you have to mow the grass, weed the garden and
                        </Text>
                        </ViewMoreText>
                    </View>

                    <View style={styles.openCloseRateSection}>
                        <Text style={styles.openCloseRateTextStyle}>
                            $40 - $120 / Person
                    </Text>
                        <Text style={styles.openCloseRateTextStyle}>
                            <Text style={{ fontWeight: '600' }}>Closed</Text>  10:00 AM - 10:00 PM
                    </Text>
                    </View>

                    <View style={styles.evaluationSection}>
                        <View style={styles.evaluationSecondSection}>
                            <View style={styles.evaluationThirdSection}>
                                <Text style={{ fontSize: Metrics.CountScale(16) }}>
                                    Evaluations (63)
                            </Text>
                            </View>
                            <TouchableOpacity onPress={() => this.viewAllRating()} style={styles.evaluationFourthSection}>
                                <Text style={{ fontSize: Metrics.CountScale(13) }}>View All  </Text>
                                <Image source={images.LeftBackIcon} resizeMode={'contain'} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.state.averageEvaluation.length != 0 &&
                        <View style={[styles.shadowEffect, styles.evaluationRatingSection]}>
                            <View>
                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={this.state.averageEvaluation[0].food}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: Metrics.CountScale(14), marginLeft: Metrics.CountScale(27) }}>Food</Text>
                                    </View>
                                </View>

                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={this.state.averageEvaluation[0].price}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: Metrics.CountScale(14), marginLeft: Metrics.CountScale(27) }}>Price</Text>
                                    </View>
                                </View>
                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={this.state.averageEvaluation[0].environment}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: Metrics.CountScale(14), marginLeft: Metrics.CountScale(27) }}>Environment</Text>
                                    </View>
                                </View>

                                <View style={styles.evaluationRatingInnerViewSection}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        <StarRating
                                            disabled={true}
                                            maxStars={5}
                                            starSize={17}
                                            halfStarEnabled
                                            halfStar={'star-half-o'}
                                            emptyStar={'star'}
                                            fullStar={'star'}
                                            fullStarColor={Colors.ORANGE}
                                            rating={this.state.averageEvaluation[0].waiterService}
                                        />
                                    </View>
                                    <View style={{ flex: 1 }}>
                                        <Text style={{ fontSize: Metrics.CountScale(14), marginLeft: Metrics.CountScale(27) }}>Waiter Service</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                    }
                    {this.state.myReview.length == 0 ?
                        <TouchableOpacity onPress={() => this.giveReview()} style={[styles.shadowEffect, styles.addMyReviewSection]}>
                            <View style={styles.addMyReviewSecondSection}>
                                <Image source={images.LeftBackIcon} resizeMode={'contain'} />
                                <Text style={{ fontSize: Metrics.CountScale(16), marginLeft: Metrics.CountScale(10) }}>Add My Review</Text>
                            </View>
                        </TouchableOpacity> :
                        <View style={styles.addMyReviewSection}></View>
                    }
                    <View style={styles.myReviewSection}>
                        <Text style={{ fontSize: Metrics.CountScale(16) }}>My Review</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: Metrics.CountScale(10) }}>
                            <View style={{ justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                                <StarRating
                                    disabled={true}
                                    maxStars={5}
                                    starSize={17}
                                    starStyle={{ paddingRight: 3 }}
                                    halfStarEnabled
                                    halfStar={'star-half-o'}
                                    emptyStar={'star'}
                                    fullStar={'star'}
                                    fullStarColor={Colors.ORANGE}
                                    rating={this.state.myReview.length != 0 ? this.state.myReview[0].ratings : null}
                                />
                                {this.state.myReview.length != 0 &&
                                    <Text style={{ fontSize: Metrics.CountScale(15), marginLeft: Metrics.CountScale(5) }}>{this.state.myReview[0].ratings}</Text>
                                }
                            </View>
                            <TouchableOpacity disabled={this.state.myReview.length == 0 ? false : true} onPress={() => this.giveReview()} style={{ padding: 15 }}>
                                <Image source={images.GrayMoreIcon} resizeMode={'center'} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {this.state.myReview.length != 0 &&
                        <View style={styles.reviewDisplaySection}>
                            <View style={styles.reviewDisplaySecondSection}>
                                <Image source={images.ProfileImage} resizeMode='cover' style={styles.recommendUserImageList} />
                                <Text style={{ fontSize: Metrics.CountScale(16), color: Colors.WHITE, marginLeft: 8 }}>dd</Text>
                            </View>
                            <View style={{ marginHorizontal: 13, marginBottom: 10 }}>
                                <Text style={styles.reviewDisplayTextStyle}>
                                    {this.state.myReview[0].description}
                                </Text>
                            </View>
                        </View>
                    }

                    <View style={styles.mapDisplaySection}>
                        {this.state.displayMap &&
                            <MapView
                                style={styles.map}
                                initialRegion={{
                                    latitude: 21.190770,
                                    longitude: 72.794010,
                                    latitudeDelta: 0.025,
                                    longitudeDelta: 0.025
                                }}
                                zoomTapEnabled={false}
                                zoomEnabled={false}
                                rotateEnabled={false}
                                scrollEnabled={false}
                            >
                                <Marker
                                    image={images.MarkerIcon}
                                    coordinate={{
                                        latitude: 21.190770,
                                        longitude: 72.794010,
                                    }}
                                />
                            </MapView>
                        }
                    </View>

                    <View style={styles.buttonSectionBottom}>
                        <TouchableOpacity onPress={() => this.addAsVisited()} style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.NAVIGATIONLABEL, marginRight: 10 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Visited
                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.followerList()} style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.RECOMMENDEDBUTTON, marginLeft: 10 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Recommend
                        </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerViewStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: Platform.OS == 'ios' ? Metrics.CountScale(20) : Metrics.CountScale(0)
    },
    headerImageStyle: {
        padding: Metrics.CountScale(15)
    },
    imageBackgroundStyle: {
        width: Metrics.screenWidth,
        height: Metrics.stopperImage,
        justifyContent: 'flex-end'
    },
    restaurantNameTextStyle: {
        margin: Metrics.CountScale(15),
        width: Metrics.CountScale(250),
        marginBottom: Metrics.CountScale(0)
    },
    likeRestaurantView: {
        margin: Metrics.CountScale(15),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    likeView: {
        flexDirection: 'row',
        flex: 0.2,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    restaurantTagView: {
        height: Metrics.CountScale(30),
        backgroundColor: Colors.NAVIGATIONLABEL,
        borderRadius: Metrics.CountScale(12),
        alignSelf: 'flex-start',
        justifyContent: 'center',
        marginVertical: Metrics.CountScale(7),
        marginRight: Metrics.CountScale(14)
    },
    restaurantTagTextStyle: {
        marginHorizontal: Metrics.CountScale(10),
        alignSelf: 'center',
        color: Colors.WHITE,
        fontSize: Metrics.CountScale(14)
    },
    shadowEffect: {
        shadowOpacity: 1,
        shadowOffset: { height: 4, width: 0 },
        elevation: 5,
        shadowColor: 'gray',
        shadowRadius: 5
    },
    recommendUserImageList: {
        height: Metrics.CountScale(28),
        width: Metrics.CountScale(28),
        borderRadius: Metrics.CountScale(28) / 2,
        borderWidth: 1,
        borderColor: Colors.WHITE
    },
    recommendedUserSection: {
        flex: 1,
        paddingTop: Metrics.CountScale(20)
    },
    RecommendUserTextStyle: {
        color: Colors.BLACK,
        paddingLeft: Metrics.CountScale(20),
        fontSize: Metrics.CountScale(15)
    },
    tagSection: {
        flex: 1,
        padding: Metrics.CountScale(20)
    },
    restaurantDescriptionSection: {
        flex: 1,
        paddingHorizontal: Metrics.CountScale(20)
    },
    restaurantDescriptionTextStyle: {
        flexWrap: 'wrap',
        fontSize: Metrics.CountScale(14),
        lineHeight: 21
    },
    openCloseRateSection: {
        flex: 1,
        padding: Metrics.CountScale(20),
        paddingBottom: 5
    },
    openCloseRateTextStyle: {
        fontSize: Metrics.CountScale(16),
        marginVertical: Metrics.CountScale(5)
    },
    evaluationSection: {
        flex: 1,
        paddingHorizontal: Metrics.CountScale(20)
    },
    evaluationSecondSection: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    evaluationThirdSection: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    evaluationFourthSection: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: Metrics.CountScale(15),
        paddingRight: 0
    },
    evaluationRatingSection: {
        flex: 1,
        marginHorizontal: Metrics.CountScale(20),
        paddingVertical: 10,
        borderRadius: Metrics.CountScale(10),
        backgroundColor: 'white'
    },
    evaluationRatingInnerViewSection: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginHorizontal: 10,
        marginVertical: 13
    },
    addMyReviewSection: {
        flex: 1,
        margin: Metrics.CountScale(20),
        borderRadius: Metrics.CountScale(10),
        backgroundColor: 'white'
    },
    addMyReviewSecondSection: {
        alignItems: 'center',
        flexDirection: 'row',
        padding: Metrics.CountScale(13),
        justifyContent: 'center'
    },
    myReviewSection: {
        flex: 1,
        padding: Metrics.CountScale(20),
        paddingTop: Metrics.CountScale(5),
        paddingRight: Metrics.CountScale(6),
        paddingBottom: Metrics.CountScale(0)
    },
    reviewDisplaySection: {
        flex: 1,
        marginHorizontal: Metrics.CountScale(10),
        borderRadius: 15,
        backgroundColor: '#2B2C2A'
    },
    reviewDisplaySecondSection: {
        flexDirection: 'row',
        marginHorizontal: 13,
        marginVertical: 10,
        alignItems: 'center'
    },
    reviewDisplayTextStyle: {
        flexWrap: 'wrap',
        fontSize: Metrics.CountScale(14),
        lineHeight: 21, color: Colors.WHITE
    },
    buttonSectionBottom: {
        flex: 1,
        padding: Metrics.CountScale(20),
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingBottom: Metrics.CountScale(20)
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(48),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    buttonSectionBottomTextStyle: {
        fontSize: Metrics.CountScale(16),
        color: Colors.WHITE
    },
    mapDisplaySection: {
        flex: 1,
        marginTop: Metrics.CountScale(20),
        height: Metrics.CountScale(250),
        width: '100%',
        backgroundColor: 'gray'
    },
    map: {
        height: Metrics.CountScale(250),
        width: '100%'
    }
});

export default RestaurantDetail;