import React from 'react';
import { View, StyleSheet, TextInput, Image, SafeAreaView, TouchableOpacity, Text, Platform, FlatList, Modal } from 'react-native';
import FoodTypeView from '../components/common/FoodTypeView';
import MapView, { Marker } from 'react-native-maps';
import { images, Metrics, Colors } from '../assets/index';

const items = [
    {
        name: 'Healthy',
        flag: false,
        media: images.HealthyIcon
    },
    {
        name: 'Italian',
        flag: false,
        media: images.ItalianIcon
    },
    {
        name: 'Arabic',
        flag: false,
        media: images.ArabicIcon
    },
    {
        name: 'Japanese',
        flag: false,
        media: images.JapaneseIcon
    },
    {
        name: 'Chinese',
        flag: false,
        media: images.ChineseIcon
    },
    {
        name: 'Asian',
        flag: false,
        media: images.AsianIcon
    },
    {
        name: 'Burger',
        flag: false,
        media: images.BurgerIcon
    },
    {
        name: 'Sandwiches',
        flag: false,
        media: images.SandwichesIcon
    },
    {
        name: 'Brazilian',
        flag: false,
        media: images.BrazilianIcon
    },
    {
        name: 'Coffee',
        flag: false,
        media: images.CoffeeIcon
    },
    {
        name: 'Pizza',
        flag: false,
        media: images.PizzaIcon
    },
    {
        name: 'Korean',
        flag: false,
        media: images.KoreanIcon
    },
    {
        name: 'Bakery',
        flag: false,
        media: images.BakeryIcon
    },
    {
        name: 'Indian',
        flag: false,
        media: images.IndianIcon
    },
    {
        name: 'Barbecue',
        flag: false,
        media: images.BarbecueIcon
    },
    {
        name: 'SeaFood',
        flag: false,
        media: images.SeaFoodIcon
    },
    {
        name: 'Portuguese',
        flag: false,
        media: images.PortugueseIcon
    },
    {
        name: 'German',
        flag: false,
        media: images.GermanIcon
    }
];

const users = [
    {
        name: 'Hariom Raje',
    },
    {
        name: 'Rohan Mali',
    },
    {
        name: 'Nitin Ghadiya',
    },
    {
        name: 'Monica Vekariya',
    }
];

const location = [
    {
        latitude: 21.190770,
        longitude: 72.794010,
    },
    {
        latitude: 21.184982,
        longitude: 72.784595,
    },
    {
        latitude: 21.187223,
        longitude: 72.829914
    }
];
const DEFAULT_PADDING = { top: 50, left: 50, bottom: 50, right: 50 };

class Explore extends React.Component {

    state = {
        modalVisible: false,
        locationModalVisible: false,
        result: items,
        displayMap: false
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ displayMap: true });
        }, 1500);
        setTimeout(() => {
            this.mapRef.fitToCoordinates(location, {

                edgePadding: DEFAULT_PADDING,
                animated: true,
            });
        }, 3000)
    }

    setModalVisible() {
        this.setState({ modalVisible: true });
    }

    setLocationModalVisible() {
        console.log('11111')
        this.setState({ locationModalVisible: true });
    }

    async setSelectSymbol(index) {
        if (this.state.result[index].flag == false) {
            this.state.result[index].flag = true;
        } else {
            this.state.result[index].flag = false;
        }
        let arr = this.state.result;
        await this.setState({ result: arr });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.mapDisplaySection}>
                    {this.state.displayMap &&
                        <MapView
                            style={styles.map}
                            ref={(ref) => this.mapRef = ref}

                            initialRegion={{
                                latitude: 21.190770,
                                longitude: 72.794010,
                                latitudeDelta: 0.025,
                                longitudeDelta: 0.025
                            }}
                        >
                            {location.map(inboxObj => (
                                <Marker
                                    image={images.MarkerIcon}
                                    coordinate={inboxObj}
                                    onPress={() => this.setLocationModalVisible()}
                                />
                            ))}
                        </MapView>
                    }
                </View>
                <View style={{ position: 'absolute', zIndex: 3, width: '100%' }}>
                    <View style={styles.searchBackgroundView}>
                        <View style={[styles.searchInputStyle, styles.shadowEffect]}>
                            <View style={{ justifyContent: 'center', paddingLeft: Metrics.CountScale(12) }}>
                                <Image source={images.SearchIcon} resizeMode='center' />
                            </View>
                            <View style={styles.textinputViewStyle}>
                                <TextInput
                                    placeholder="Foods, restaurants, friends.."
                                    placeholderTextColor={Colors.GRAY}
                                    selectionColor={Colors.BLACK}
                                    style={styles.textinputStyle}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.buttonSectionBottom}>
                        <TouchableOpacity onPress={() => this.setModalVisible()} style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.NAVIGATIONLABEL, marginRight: 8 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Food Types
                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setLocationModalVisible()} style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.VIBESBUTTON, marginLeft: 8 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Vibes
                        </Text>
                        </TouchableOpacity>
                    </View>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => { this.setState({ modalVisible: false }) }}
                    >
                        <TouchableOpacity activeOpacity={1} onPress={() => { this.setState({ modalVisible: false }) }} style={{ flex: 1, justifyContent: 'flex-end' }} >
                            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#F3F3F3', height: Metrics.screenHeight / 1.5 }}>
                                <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
                                    <View style={{ padding: 20, paddingBottom: 0, flex: 1 }}>
                                        <FlatList
                                            data={items}
                                            contentContainerStyle={{
                                                flexDirection: 'row',
                                                flexWrap: 'wrap',
                                                justifyContent: 'space-between'
                                            }}
                                            renderItem={({ item, index }) => <FoodTypeView data={item} setSelectSymbol={() => this.setSelectSymbol(index)} />}
                                            showsVerticalScrollIndicator={false}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    </Modal>
                </View>

                <View style={{ position: 'absolute', zIndex: 3, bottom: this.state.locationModalVisible == true ? 130 : 0, right: 0 }}>
                    <View>
                        <Image source={images.ExploreShareIcon} resizeMode='cover' />
                    </View>
                </View>

                <View style={{ position: 'absolute', zIndex: 3, width: '100%', bottom: 0 }}>

                    {this.state.locationModalVisible == true &&
                        <View style={{ justifyContent: 'flex-end', paddingHorizontal: Metrics.CountScale(20), paddingBottom: Metrics.CountScale(20), marginBottom: Platform.OS == 'ios' ? Metrics.CountScale(15) : Metrics.CountScale(15) }} >
                            <View style={[styles.shadowEffect, { backgroundColor: Colors.WHITE, height: ((Metrics.screenWidth / 2.3) / 1.49), borderRadius: Metrics.CountScale(10) }]}>
                                <TouchableOpacity onPress={() => { this.setState({ locationModalVisible: false }) }} style={{ flexDirection: 'row' }}>
                                    <View>
                                        <Image source={images.PopupExploreImage} resizeMode='cover' style={{ width: Metrics.screenWidth / 2.3, height: ((Metrics.screenWidth / 2.3) / 1.49) }} />
                                    </View>
                                    <View style={{ padding: 15, flex: 1, flexWrap: 'wrap' }}>
                                        <Text numberOfLines={2} style={{ fontSize: Metrics.CountScale(17) }}>
                                            Jackson Market and Deli
                                    </Text>
                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1 }}>
                                            <View style={styles.recommendListView}>
                                                {users.map((inboxObj, index) => (
                                                    <View style={{ width: Metrics.CountScale(50), marginRight: Metrics.CountScale(-27), zIndex: -index + 4 }}>
                                                        {index <= 2 &&
                                                            <View style={styles.commonAbsolute}>
                                                                <Image source={images.HomeImage} resizeMode={'cover'} style={styles.recommendList} />
                                                            </View>
                                                        }
                                                    </View>
                                                ))}
                                                <View style={{ width: Metrics.CountScale(30), marginRight: Metrics.CountScale(10), zIndex: 1 }}>
                                                    {users.length > 3 &&
                                                        <View></View>
                                                    }
                                                </View>
                                            </View>
                                            <TouchableOpacity style={{ paddingHorizontal: 10, flex: 0.1, justifyContent: 'center', alignItems: 'center' }}>
                                                <Image source={images.GrayMoreIcon} resizeMode='contain' />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 1
    },
    textinputViewStyle: {
        flex: 1,
        padding: Metrics.CountScale(15),
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        paddingLeft: Metrics.CountScale(12)
    },
    textinputStyle: {
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: Metrics.CountScale(15)
    },
    searchBackgroundView: {
        paddingBottom: Metrics.CountScale(15),
        padding: Metrics.CountScale(15),
        paddingTop: Metrics.CountScale(0),
        backgroundColor: 'transparent',
        // zIndex: 3,
        // position: 'absolute',
        // width: '100%',
        paddingTop: Platform.OS == 'ios' ? Metrics.CountScale(55) : Metrics.CountScale(20)
    },
    searchInputStyle: {
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12),
        padding: Metrics.CountScale(5)
    },
    shadowEffect: {
        shadowOpacity: 0.9,
        shadowOffset: { height: 2 },
        elevation: 5,
        shadowColor: Colors.GRAY
    },
    buttonSectionBottom: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Metrics.CountScale(15),
        paddingVertical: Metrics.CountScale(10)
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(45),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    buttonSectionBottomTextStyle: {
        fontSize: Metrics.CountScale(16),
        color: Colors.WHITE
    },
    commonAbsolute: {
    },
    shadowEffect: {
        shadowOpacity: 0.2,
        shadowOffset: { height: 2 },
        elevation: 5,
        shadowColor: Colors.GRAY
    },
    recommendList: {
        width: Metrics.CountScale(30),
        height: Metrics.CountScale(30),
        borderColor: Colors.WHITE,
        borderWidth: 1,
        borderRadius: Metrics.CountScale(30) / 2
    },
    recommendListView: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1
    },
    mapDisplaySection: {
        flex: 1,
        backgroundColor: 'gray',
        zIndex: 2
    },
    map: {
        height: '100%',
        width: '100%'
    }
});

export default Explore;