import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, Image, Modal, SafeAreaView, FlatList, Platform } from 'react-native';
import FoodTypeView from '../components/common/FoodTypeView';
import { images, Metrics, Colors } from '../assets/index';

const items = [
    {
        name: 'Healthy',
        flag: false,
        media: images.HealthyIcon
    },
    {
        name: 'Italian',
        flag: false,
        media: images.ItalianIcon
    },
    {
        name: 'Arabic',
        flag: false,
        media: images.ArabicIcon
    },
    {
        name: 'Japanese',
        flag: false,
        media: images.JapaneseIcon
    },
    {
        name: 'Chinese',
        flag: false,
        media: images.ChineseIcon
    },
    {
        name: 'Asian',
        flag: false,
        media: images.AsianIcon
    },
    {
        name: 'Burger',
        flag: false,
        media: images.BurgerIcon
    },
    {
        name: 'Sandwiches',
        flag: false,
        media: images.SandwichesIcon
    },
    {
        name: 'Brazilian',
        flag: false,
        media: images.BrazilianIcon
    },
    {
        name: 'Coffee',
        flag: false,
        media: images.CoffeeIcon
    },
    {
        name: 'Pizza',
        flag: false,
        media: images.PizzaIcon
    },
    {
        name: 'Korean',
        flag: false,
        media: images.KoreanIcon
    },
    {
        name: 'Bakery',
        flag: false,
        media: images.BakeryIcon
    },
    {
        name: 'Indian',
        flag: false,
        media: images.IndianIcon
    },
    {
        name: 'Barbecue',
        flag: false,
        media: images.BarbecueIcon
    },
    {
        name: 'SeaFood',
        flag: false,
        media: images.SeaFoodIcon
    },
    {
        name: 'Portuguese',
        flag: false,
        media: images.PortugueseIcon
    },
    {
        name: 'German',
        flag: false,
        media: images.GermanIcon
    }
];

class AskForRecommendation extends React.Component {

    state = {
        modalVisible: false,
        result: items
    }

    setModalVisible() {
        this.setState({ modalVisible: true });
    }

    async setSelectSymbol(index) {
        if (this.state.result[index].flag == false) {
            this.state.result[index].flag = true;
        } else {
            this.state.result[index].flag = false;
        }
        let arr = this.state.result;
        await this.setState({ result: arr });
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={{ padding: Metrics.CountScale(15) }}>
                    <View style={{ marginBottom: Metrics.CountScale(17), marginHorizontal: Metrics.CountScale(7) }}>
                        <Text style={{ fontSize: Metrics.CountScale(21), color: Colors.WHITE }}>
                            What are you looking for?
                    </Text>
                    </View>
                    <View style={[styles.textinputViewStyle, { height: Metrics.CountScale(125) }]}>
                        <TextInput
                            placeholder="Ask your followers and influencers for recommendations on which restaurants to go such as “What is the best place to have lunch with kids in Sao Paulo?”"
                            placeholderTextColor={Colors.GRAY}
                            selectionColor={Colors.BLACK}
                            multiline
                            textAlignVertical={'top'}
                            style={styles.textinputStyle}
                        />
                    </View>

                    <View style={{ paddingVertical: Metrics.CountScale(20) }}>
                        <View style={{ flexDirection: 'row', backgroundColor: Colors.WHITE, borderRadius: Metrics.CountScale(12) }}>
                            <View style={{ justifyContent: 'center', paddingLeft: Metrics.CountScale(12) }}>
                                <Image source={images.GrayLocation} resizeMode='center' />
                            </View>
                            <View style={[styles.textinputViewStyle, { flex: 1, paddingLeft: Metrics.CountScale(10) }]}>
                                <TextInput
                                    placeholder="Add a location"
                                    placeholderTextColor={Colors.GRAY}
                                    selectionColor={Colors.BLACK}
                                    style={styles.textinputStyle}
                                />
                            </View>
                        </View>
                    </View>

                    <View style={styles.buttonSectionBottom}>
                        <TouchableOpacity onPress={() => this.setModalVisible()} style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.NAVIGATIONLABEL, marginRight: 10 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Food Types
                        </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, { backgroundColor: Colors.VIBESBUTTON, marginLeft: 10 }]}>
                            <Text style={styles.buttonSectionBottomTextStyle}>
                                Vibes
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => { this.setState({ modalVisible: false }) }}
                    >
                        <TouchableOpacity activeOpacity={1} onPress={() => { this.setState({ modalVisible: false }) }} style={{ flex: 1, justifyContent: 'flex-end' }} >
                            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#F3F3F3', height: Metrics.screenHeight / 1.5 }}>
                                <TouchableOpacity onPress={() => { this.setState({ modalVisible: false }) }}>
                                    <View style={{ padding: 20, paddingBottom: 0, flex: 1 }}>
                                        <FlatList
                                            data={items}
                                            contentContainerStyle={{
                                                flexDirection: 'row',
                                                flexWrap: 'wrap',
                                                justifyContent: 'space-between'
                                            }}
                                            renderItem={({ item, index }) => <FoodTypeView data={item} setSelectSymbol={() => this.setSelectSymbol(index)} />}
                                            showsVerticalScrollIndicator={false}
                                        />
                                        <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, styles.applyButtonViewStyle]}>
                                            <Text style={styles.buttonSectionBottomTextStyle}>
                                                Apply
                                            </Text>
                                        </TouchableOpacity>
                                    </View>

                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                    </Modal>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2B2C2A',
    },
    textinputViewStyle: {
        padding: Metrics.CountScale(15),
        backgroundColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(12)
    },
    textinputStyle: {
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: Metrics.CountScale(16)
    },
    buttonSectionBottom: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(45),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    applyButtonViewStyle: {
        height: Metrics.CountScale(60),
        backgroundColor: Colors.NAVIGATIONLABEL,
        zIndex: 1,
        position: 'absolute',
        bottom: Platform.OS == 'ios' ? Metrics.CountScale(50) : Metrics.CountScale(20),
        alignSelf: 'center',
        width: '90%'
    },
    buttonSectionBottomTextStyle: {
        fontSize: Metrics.CountScale(16),
        color: Colors.WHITE
    },
});

export default AskForRecommendation;