import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ImageBackground, Image, Platform, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { images, Metrics, Colors } from '../assets/index';
import APICaller from '../api/Api';
import { LoadWheel } from '../components/common/LoadWheel';

let header = '';
let token = '';

class Registration extends React.Component {

    state = {
        email_id: '',
        password: '',
        confirm_password: '',
        loading: false
    };

    login() {
        this.props.navigation.navigate('Login');
    }
    registration() {
        if (this.state.email_id.trim() == '') {
            alert('Please enter email.')
        }
        else if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email_id) == false) {
            alert('Please enter valid email.')
        }
        else if (this.state.password.trim() == '') {
            alert('Please enter password.')
        }
        else if (this.state.password.length < 6 || this.state.password.length > 16) {
            alert('Password should be at least 6-15 characters.')
        }
        else if (this.state.confirm_password.trim() == '') {
            alert('Please enter confirm password.')
        }
        else if (this.state.confirm_password.length < 6 || this.state.confirm_password.length > 16) {
            alert('Confirm password should be at least 6-15 characters.')
        }
        else if (this.state.password !== this.state.confirm_password) {
            alert('Password and Confirm Password does not match.')
        }
        else {

            this.setState({ loading: true });

            var formData = new FormData();

            formData.append("email", this.state.email_id);
            formData.append("password", this.state.password);

            APICaller('users/api/v1/users/registration', 'post', token, formData, header).then(async (json) => {
                console.log(json);

                this.setState({ loading: false });

                if (json.status == 'success') {
                    this.props.navigation.navigate('FoodType');
                }
                else {
                    alert('Email_id already exist.')
                }
            })
                .catch((error) => {
                    this.setState({ loading: false });
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }
    facebookLogin() {
        this.props.navigation.navigate('TabBar');
    }
    gmailLogin() {
        this.props.navigation.navigate('TabBar');
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={images.BlurBackground} style={{ flex: 1 }} resizeMode={'cover'} >
                    <KeyboardAwareScrollView
                        style={{ flex: 1 }}
                    >
                        <View style={{ marginTop: Metrics.CountScale(78), alignItems: 'center', justifyContent: 'center', paddingTop: Platform.OS == 'ios' ? (Metrics.screenHeight == 812 ? 44 : 20) : 0 }}>
                            <Image source={images.AppLogo} resizeMode={'cover'} />
                        </View>

                        <View style={{ padding: Metrics.CountScale(25), marginTop: Metrics.CountScale(20) }}>
                            <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', borderRadius: Metrics.CountScale(40) }}>
                                <TextInput
                                    placeholder="EMAIL ADDRESS"
                                    placeholderTextColor={Colors.WHITE}
                                    selectionColor={Colors.WHITE}
                                    style={styles.textinputStyle}
                                    value={this.state.email_id}
                                    onChangeText={val => this.setState({ email_id: val })}
                                />
                            </View>

                            <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', borderRadius: Metrics.CountScale(40), marginTop: Metrics.CountScale(15) }}>
                                <TextInput
                                    placeholder="PASSWORD"
                                    placeholderTextColor={Colors.WHITE}
                                    selectionColor={Colors.WHITE}
                                    style={styles.textinputStyle}
                                    secureTextEntry
                                    value={this.state.password}
                                    onChangeText={val => this.setState({ password: val })}
                                />
                            </View>

                            <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', borderRadius: Metrics.CountScale(40), marginTop: Metrics.CountScale(15) }}>
                                <TextInput
                                    placeholder="CONFIRM PASSWORD"
                                    placeholderTextColor={Colors.WHITE}
                                    selectionColor={Colors.WHITE}
                                    style={styles.textinputStyle}
                                    secureTextEntry
                                    value={this.state.confirm_password}
                                    onChangeText={val => this.setState({ confirm_password: val })}
                                />
                            </View>

                            <TouchableOpacity onPress={() => { this.registration() }} style={[styles.loginButtonstyle, { marginTop: Metrics.CountScale(15) }]}>
                                <Text style={{ fontSize: Metrics.CountScale(15), }}>
                                    SIGN UP
                            </Text>
                            </TouchableOpacity>

                            <View style={{ marginTop: Metrics.CountScale(15), justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: Metrics.CountScale(14), color: Colors.WHITE }}>
                                    By singing up, you agree to Gorjeta's
                            </Text>
                            </View>
                            <TouchableOpacity style={{ alignSelf: 'center', padding: Metrics.CountScale(10), paddingTop: Metrics.CountScale(5) }}>
                                <Text style={{ textAlign: 'center', fontSize: Metrics.CountScale(14), color: Colors.TANDC }}>Terms and Condition of Use.</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: Metrics.CountScale(60) }}>
                            <View style={styles.buttonSectionBottom}>
                                <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, { marginRight: Metrics.CountScale(10) }]}>
                                    <Image source={images.FbButton} resizeMode={'cover'} />
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.buttonSectionBottomViewStyle, { marginLeft: Metrics.CountScale(10) }]}>
                                    <Image source={images.GmailButton} resizeMode={'cover'} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                <Text style={{ color: Colors.WHITE, fontSize: Metrics.CountScale(15) }}>Already have an account?</Text>
                                <TouchableOpacity onPress={() => { this.login() }} style={{ padding: Metrics.CountScale(10) }}>
                                    <Text style={{ color: Colors.SIGNUP, fontWeight: 'bold' }}>LOGIN</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
                <LoadWheel visible={this.state.loading} onRequestClose={() => this.setState({ loading: false })} style={{ flex: 1 }} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    textinputStyle: {
        paddingTop: Metrics.CountScale(0),
        paddingBottom: Metrics.CountScale(0),
        height: Metrics.CountScale(50),
        width: '100%',
        textAlign: 'center',
        color: Colors.WHITE,
        borderRadius: Metrics.CountScale(40),
        paddingHorizontal: Metrics.CountScale(10),
        fontSize: Metrics.CountScale(15),
    },
    loginButtonstyle: {
        backgroundColor: Colors.WHITE,
        height: Metrics.CountScale(45),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: Metrics.CountScale(40)
    },
    buttonSectionBottom: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Metrics.CountScale(32),
        paddingBottom: Metrics.CountScale(25),
        alignItems: 'flex-end'
    },
    buttonSectionBottomViewStyle: {
        height: Metrics.CountScale(45),
        width: '100%',
        borderRadius: Metrics.CountScale(30),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
});
export default Registration;