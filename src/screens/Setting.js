import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Metrics, Colors, images } from '../assets/index';

class Setting extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: 'Settings',
        headerTitleStyle: styles.headerStyleNav,
        headerStyle: { backgroundColor: Colors.NAVIGATIONLABEL },
        headerTintColor: Colors.WHITE,
        headerRight: <View></View>
    });

    editProfile() {
        this.props.navigation.navigate('EditProfile');
    }

    friends() {
        this.props.navigation.navigate('FriendsModule');
    }

    changePassword() {
        this.props.navigation.navigate('ChangePassword');
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => this.editProfile()} style={styles.commonViewStyle}>
                    <View style={styles.commonViewStyle}>
                        <Icon name="user-circle" size={24} color={Colors.NAVIGATIONLABEL} />
                    </View>
                    <View>
                        <Text style={styles.commonTextStyle}>
                            Edit Profile
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.friends()} style={styles.commonViewStyle}>
                    <View style={styles.commonViewStyle}>
                        <Icon name="user-o" size={24} color={Colors.NAVIGATIONLABEL} />
                    </View>
                    <View>
                        <Text style={styles.commonTextStyle}> Friends</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.changePassword()} style={styles.commonViewStyle}>
                    <View style={styles.commonViewStyle}>
                        <Icon name="key" size={24} color={Colors.NAVIGATIONLABEL} />
                    </View>
                    <View>
                        <Text style={styles.commonTextStyle}>
                            Change Password
                        </Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.commonViewStyle}>
                    <View style={styles.commonViewStyle}>
                        <Icon name="sign-out" size={24} color={Colors.NAVIGATIONLABEL} />
                    </View>
                    <View>
                        <Text style={styles.commonTextStyle}> Logout</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    headerStyleNav: {
        fontSize: Metrics.CountScale(20),
        color: Colors.WHITE,
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    commonTextStyle: {
        fontSize: Metrics.CountScale(18),
        color: Colors.BLACK,
        fontWeight: '600'
    },
    commonViewStyle: {
        flexDirection: 'row',
        padding: Metrics.CountScale(15),
        alignItems: 'center'
    },
    imageViewStyle: {
        paddingHorizontal: Metrics.CountScale(12),
        paddingVertical: Metrics.CountScale(12)
    }
});

export default Setting;