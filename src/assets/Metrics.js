import { scale } from '../components/helper/function';
import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

let screenHeight = width < height ? height : width
let screenWidth = width < height ? width : height

const Metrics = {
    screenHeight: screenHeight,
    screenWidth: screenWidth,
    offerViewWidth: screenWidth - 50,
    stopperImage: (screenWidth / 1.17),
    offerImage: ((screenWidth - 50) / 2.05),
    commonImageView: ((screenWidth / 2.5) / 1.62), //1.64
    CountScale: (val) => {
        return scale(val);
    }
};

export default Metrics;
