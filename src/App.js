//LIBRARIES
import React from 'react';
import { View, StyleSheet, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';
//ASSETS
import AppNavigation from './AppNavigation';
import InternetConnectivity from './components/common/InternetConnectivity'

//====CLASS DECLARATION====//
class App extends React.Component {

    async componentDidMount() {
        console.disableYellowBox = true;
        this.getToken();
        this.getNotification();
        this.onNotificationPress();
    }

    //==========================Notification function==============// 
    getToken() {
        firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    firebase.messaging().getToken()
                        .then(async (fcmToken) => {
                            if (fcmToken) {
                                console.log(fcmToken)
                                global.fcmDeviceToken = fcmToken
                                await AsyncStorage.setItem('fcmToken', fcmToken);
                            } else {
                                console.log("You will not recieve notifications from Gorjerta!");
                            }
                        });
                }
                else {
                    firebase.messaging().requestPermission()
                        .then(enabled => {
                            firebase.messaging().getToken()
                                .then(async fcmToken => {
                                    if (fcmToken) {
                                        global.fcmDeviceToken = fcmToken
                                        await AsyncStorage.setItem('fcmToken', fcmToken);

                                    } else {
                                        console.log("You will not recieve notifications from Gorjeta!");
                                    }
                                });
                        })
                        .catch(error => {
                            console.log("You will not recieve notifications from Gorjeta!", error);
                        });
                }
            });
    }

    getNotification() {
        firebase.notifications().getInitialNotification().then(notification => {
            console.log("INITIAL NOTIFICATION", notification)
        });
    }

    onNotificationPress() {
        firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log('..............****.............****................')
            console.log(notificationOpen)
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <InternetConnectivity />
                <AppNavigation />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

export default App;