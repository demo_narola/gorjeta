const BASE_URL = 'http://192.168.1.243:8765/';

const APICaller = (endPoint, method, token, body, contentType, header) => fetch(`${BASE_URL}${endPoint}`, {
    method: method || 'get',
    body: body,
    headers: {
        'Content-Type': contentType || 'application/json',
        'Authorization': token,
        header
    },
})
    .then((response) => {
        // if (response) {
        //     return response;
        // } else {
        //     alert('Nothing');
        //     return null;
        // }
        return response.json();
    })
    .then((responseJson) => {
        // console.log(responseJson);
        // if (!responseJson || (responseJson && responseJson.status && responseJson.status != 200)) {
        //     alert(responseJson.message);
        //     return;
        // }
        return responseJson;
    })

export default APICaller;

