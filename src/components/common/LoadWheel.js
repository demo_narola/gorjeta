//LIBRAIRES
import React from 'react';
import { View } from 'react-native';
//ASSETS
import { Colors } from '../../assets/index';

var Spinner = require("react-native-spinkit");

export const LoadWheel = ({ visible, text, onRequestClose }) => {
    return (
        <View style={{ position: 'absolute', justifyContent: 'center', height: visible ? '100%' : 0, width: visible ? '100%' : 0, alignItems: 'center', zIndex: 1 }}>
            <Spinner
                style={{ justifyContent: 'center', alignItems: 'center' }}
                isVisible={visible}
                size={60}
                type={'WanderingCubes'}
                color={Colors.NAVIGATIONLABEL} />
        </View>
    )
}
