import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import { images, Metrics, Colors } from '../../assets/index';
import APICaller from '../../api/Api';

let access_Token;
let body = '';
let header = '';
let token = '';

class FollowListView extends React.Component {

    state = {
        unFollow: [],
        follow: []
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    followUnfollow(id) {

        token = 'Bearer ' + access_Token;

        if (this.props.data.following == true) {
            APICaller('users/api/v1/users/95/unfollows/49', 'put', token, body, header).then(async (json) => {
                this.setState({ unFollow: json });
                await EventRegister.emit('UpdateUnFollowList', id);
                await EventRegister.emit('UpdateProfile');
            })
                .catch((error) => {
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        } else {
            APICaller('users/api/v1/users/95/follows/80', 'put', token, body, header).then(async (json) => {
                this.setState({ follow: json });
                await EventRegister.emit('UpdateFollowerList');
                await EventRegister.emit('UpdateProfile');
            })
                .catch((error) => {
                    alert('Something went wrong, try again.')
                    console.error(error);
                });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={images.Offermage} resizeMode='cover' style={styles.restaurantImageStyle} />
                </View>
                <View style={{ flex: 0.68, marginLeft: Metrics.CountScale(10) }}>
                    <Text style={{ fontSize: Metrics.CountScale(16), fontWeight: 'bold', flex: 1 }} numberOfLines={1}>
                        {this.props.data.firstName} {this.props.data.lastName}
                    </Text>
                    {/* <Text style={{ fontSize: Metrics.CountScale(16), flex: 1 }} numberOfLines={1}>
                        Friends on Facebook
                    </Text> */}
                </View>
                <TouchableOpacity onPress={() => this.followUnfollow(this.props.data.id)} style={[styles.followButtonViewStyle, { backgroundColor: this.props.data.following == true ? Colors.VIBESBUTTON : Colors.NAVIGATIONLABEL }]}>
                    <Text style={styles.followButtonTextStyle}>
                        {this.props.data.following == true ? 'Following' : 'Follow'}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: Metrics.CountScale(22),
        flexDirection: 'row',
        flex: 1,
        marginVertical: Metrics.CountScale(18)
    },
    restaurantImageStyle: {
        borderRadius: Metrics.CountScale(42) / 2,
        width: Metrics.CountScale(42),
        height: Metrics.CountScale(42)
    },
    followButtonViewStyle: {
        height: Metrics.CountScale(30),
        width: '100%',
        borderRadius: Metrics.CountScale(25),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.32,
        alignSelf: 'center'
    },
    followButtonTextStyle: {
        fontSize: Metrics.CountScale(14),
        color: Colors.WHITE
    }
});

export default FollowListView;