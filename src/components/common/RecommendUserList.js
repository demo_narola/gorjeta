import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { images, Metrics, Colors } from '../../assets/index';

class RecommendUserList extends React.Component {

    userProfile(id) {
        this.props.navigate.navigate('Profile', { userId: id });
    }

    render() {
        return (
            <TouchableOpacity onPress={() => { this.userProfile(this.props.data.id) }} style={[styles.viewStyle, { paddingLeft: this.props.index == 0 ? (this.props.padd || 0) : 0 }]}>
                <View>
                    <Image source={images.ProfileImage} resizeMode='cover' style={styles.recommendUserImageList} />
                </View>
                <View style={{ marginTop: Metrics.CountScale(10) }}>
                    <Text numberOfLines={1} style={styles.textStyle}>{this.props.data.firstName} {this.props.data.lastName}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(12),
        width: Metrics.CountScale(90),
        textAlign: 'center'
    },
    viewStyle: {
        marginVertical: Metrics.CountScale(10),
        alignItems: 'center',
        marginHorizontal: Metrics.CountScale(6)
    },
    recommendUserImageList: {
        height: Metrics.CountScale(65),
        width: Metrics.CountScale(65),
        borderRadius: Metrics.CountScale(65) / 2,
        borderWidth: 3,
        borderColor: Colors.WHITE
    }
});

export default RecommendUserList;