import React from 'react';
import { Text, View, StyleSheet, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { images, Metrics, Colors } from '../../assets/index';

class OtherUserProfileView extends React.Component {

    restaurantDetail() {
        this.props.navigate.navigate('RestaurantDetail');
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.restaurantDetail()} >
                <View style={[styles.conatiner, { marginLeft: this.props.index == 0 ? (this.props.padd || 0) : 0 }]}>
                    <ImageBackground source={images.OtherUserRestaurantImage} resizeMode='contain' style={styles.imageBackgroundStyle} >
                        <View style={styles.textViewStyle}>
                            <Text numberOfLines={2} style={styles.textStyle}>
                                {this.props.data.name}
                            </Text>
                        </View>
                    </ImageBackground>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    conatiner: {
        borderRadius: Metrics.CountScale(10),
        marginHorizontal: Metrics.CountScale(11),
        marginTop: Metrics.CountScale(10)
    },
    imageBackgroundStyle: {
        width: Metrics.screenWidth / 2.5,
        height: ((Metrics.screenWidth / 2.5) / 1.49)
    },
    textViewStyle: {
        justifyContent: 'flex-end',
        flex: 1,
        marginHorizontal: Metrics.CountScale(15)
    },
    textStyle: {
        marginBottom: Metrics.CountScale(2),
        height: Metrics.CountScale(40),
        fontSize: Metrics.CountScale(13),
        color: Colors.WHITE,
        fontWeight: 'bold'
    }
});

export default OtherUserProfileView;