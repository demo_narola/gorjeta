import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { images, Metrics, Colors } from '../../assets/index';

class FoodTypeView extends React.Component {
    render() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={this.props.setSelectSymbol} style={[styles.container, { backgroundColor: this.props.data.flag == true ? '#F0ECE7' : Colors.WHITE }]}>
                {this.props.data.flag == true &&
                    <View style={styles.foodTypeImageStyle}>
                        <Image source={images.SelectedIcon} resizeMode={'center'} />
                    </View>
                }
                <View style={{ alignItems: 'center', flex: 1 }}>
                    <View style={{ paddingTop: Metrics.CountScale(20) }}>
                        <Image source={this.props.data.media} resizeMode={'cover'} style={styles.recommendList} />
                    </View>
                    <View style={{ paddingTop: Metrics.CountScale(25), paddingHorizontal: Metrics.CountScale(10) }}>
                        <Text style={styles.foodNameTextStyle} numberOfLines={2}>
                            {this.props.data.name}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius: Metrics.CountScale(9),
        margin: Metrics.CountScale(2),
        alignItems: 'center',
        height: ((Metrics.screenWidth - 52) / 3) * 1.50,
        width: (Metrics.screenWidth - 52) / 3
    },
    foodTypeImageStyle: {
        position: 'absolute',
        right: 0,
        top: 0,
        padding: Metrics.CountScale(8)
    },
    foodNameTextStyle: {
        textAlign: 'center',
        color: Colors.BLACK,
        fontSize: Metrics.CountScale(14)
    },
    recommendList: {
        width: Metrics.CountScale(60),
        height: Metrics.CountScale(60),
        borderRadius: Metrics.CountScale(60) / 2
    },
});

export default FoodTypeView;