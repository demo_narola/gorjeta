import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground, AsyncStorage } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import { images, Metrics, Colors } from '../../assets/index';
import APICaller from '../../api/Api';

let access_Token;
let header = '';
let token = '';

class FoodFeedview extends React.Component {

    state = {
        recommend: false,
        want: false,
        top: false,
        result: []
    }

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    recommend() {
        if (this.state.recommend == false) {
            this.setState({ recommend: true });
        } else {
            this.setState({ recommend: false });
        }
    }

    want(id) {

        token = 'Bearer ' + access_Token;

        var formData = new FormData();

        formData.append("loginId", 53);
        formData.append("restaurantId", id);

        APICaller('users/api/v1/users/wishtogo-restaurant', 'post', token, formData, 'multipart/form-data', header).then(async (json) => {
            console.log(json);
            if (json.status == 'success') {
                console.log('in')
                await EventRegister.emit('UpdateFoodFeedData', id);
                await EventRegister.emit('UpdateProfile');
            }
        })
            .catch((error) => {
                alert('Something went wrong, try again.')
                console.error(error);
            });
    }

    top() {
        if (this.state.top == false) {
            this.setState({ top: true });
        } else {
            this.setState({ top: false });
        }
    }

    render() {
        return (
            <View style={{ paddingBottom: Metrics.CountScale(15) }}>
                <ImageBackground resizeMode={'cover'} source={images.FoodFeedImage} style={styles.imageBackgroundStyle}>
                    <View style={{ justifyContent: 'space-between', flex: 1, justifyContent: 'flex-end' }}>
                        <View>
                            <View style={styles.restaurantNameTextStyle}>
                                <Text numberOfLines={2} style={{ fontSize: Metrics.CountScale(25), color: Colors.WHITE }}>{this.props.data.restaurantName}</Text>
                            </View>
                            <View style={styles.likeRestaurantView}>
                                <View style={{ flexDirection: 'row', flex: 0.8 }}>
                                    <Image source={images.WhiteLocation} resizeMode={'contain'} style={{ marginRight: Metrics.CountScale(8) }} />
                                    <Text numberOfLines={1} style={{ fontSize: Metrics.CountScale(12), color: Colors.WHITE }}>{this.props.data.restaurantAddress}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ImageBackground>
                <View style={styles.userContainer}>
                    <View>
                        <Image source={images.ProfileImage} resizeMode='cover' style={styles.restaurantImageStyle} />
                    </View>
                    <View style={{ flex: 1, marginLeft: Metrics.CountScale(10) }}>
                        <Text style={{ fontSize: Metrics.CountScale(15), flex: 1 }} numberOfLines={1}>
                            {this.props.data.firstName} {this.props.data.lastName}
                        </Text>
                        <Text style={{ fontSize: Metrics.CountScale(12), flex: 1 }} numberOfLines={1}>
                            Internet Banner Advertising Most Reliable Form Advertising
                            </Text>
                    </View>
                    <TouchableOpacity style={styles.followButtonViewStyle}>
                        <Image source={images.BlackMoreIcon} resizeMode='cover' />
                    </TouchableOpacity>
                </View>
                <View style={styles.likeContainer}>
                    <View style={{ flex: 0.97 }}>
                        <TouchableOpacity onPress={() => { this.recommend() }} style={[styles.commonTouchableStyle, { width: Metrics.CountScale(120) }]}>
                            <Image source={this.props.data.recommonded == false ? images.RecommendIcon : images.ActiveLikeIcon} resizeMode='cover' />
                            <Text style={styles.recommendedTextStyle} numberOfLines={1}>
                                Recommend
                                </Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => { this.want(this.props.data.restaurantId) }} style={[styles.commonTouchableStyle, { marginRight: 15 }]}>
                        <Image source={this.props.data.wishListed == false ? images.InactiveLikeIcon : images.DarkHeartIcon} resizeMode='cover' />
                        <Text style={styles.commonTextStyle} numberOfLines={1}>
                            {this.props.data.wishListCount == 0 ? null : this.props.data.wishListCount}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.top() }} style={[styles.commonTouchableStyle, { marginRight: 15 }]}>
                        <Image source={this.state.top == false ? images.InactiveBookmarkIcon : images.BookmarkIcon} resizeMode='cover' />
                        <Text style={styles.commonTextStyle} numberOfLines={1}>
                            120
                            </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.commonTouchableStyle, { paddingHorizontal: Metrics.CountScale(5) }]}>
                        <Image source={images.ShareIcon} resizeMode='cover' />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    recommendedUserSection: {
        flex: 1,
        paddingTop: Metrics.CountScale(15),
    },
    imageBackgroundStyle: {
        width: Metrics.screenWidth,
        height: Metrics.screenWidth / 1.64
    },
    restaurantNameTextStyle: {
        margin: Metrics.CountScale(15),
        width: Metrics.CountScale(250),
        marginBottom: Metrics.CountScale(0)
    },
    likeRestaurantView: {
        margin: Metrics.CountScale(15),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userContainer: {
        marginLeft: Metrics.CountScale(18),
        flexDirection: 'row',
        flex: 1,
        marginVertical: Metrics.CountScale(18)
    },
    restaurantImageStyle: {
        borderRadius: Metrics.CountScale(42) / 2,
        width: Metrics.CountScale(42),
        height: Metrics.CountScale(42),
        borderColor: Colors.WHITE
    },
    followButtonViewStyle: {
        paddingHorizontal: Metrics.CountScale(10),
        paddingVertical: Metrics.CountScale(5),
        marginTop: Metrics.CountScale(-5),
        marginRight: Metrics.CountScale(10)
    },
    followButtonTextStyle: {
        fontSize: Metrics.CountScale(14),
        color: Colors.WHITE
    },
    likeContainer: {
        marginLeft: Metrics.CountScale(30),
        flexDirection: 'row',
        flex: 1
    },
    commonTouchableStyle: {
        flexDirection: 'row',
        paddingVertical: Metrics.CountScale(5),
        alignItems: 'center'
    },
    commonTextStyle: {
        fontSize: Metrics.CountScale(8),
        marginLeft: Metrics.CountScale(4),
        alignSelf: 'flex-end'
    },
    recommendedTextStyle: {
        fontSize: Metrics.CountScale(14),
        flex: 1,
        marginLeft: Metrics.CountScale(10)
    }
});

export default FoodFeedview;