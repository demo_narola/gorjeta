import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import { images, Metrics, Colors } from '../../assets/index';
import APICaller from '../../api/Api';

let access_Token;
let header = '';
let token = '';

class RestaurantListView extends React.Component {

    async componentDidMount() {

        await AsyncStorage.getItem('ACCESS_TOKEN', async (err, jsonResult) => {
            access_Token = jsonResult;
        });
    }

    removeFromWantToGo(id) {

        var formData = new FormData();

        formData.append('userId', 56);
        formData.append('restaurantId', id);

        token = 'Bearer ' + access_Token;

        console.log(formData);

        APICaller('users/api/v1/users/remove-from-wishlist', 'delete', token, formData, header).then(async (json) => {
            if (json.status == 'success') {
                await EventRegister.emit('UpdateWantToGoList', id);
                await EventRegister.emit('UpdateProfile');
            }
            else {
                alert('Something went wrong, try again.');
            }
        })
            .catch((error) => {
                alert('Something went wrong, try again.')
                console.error(error);
            });

    }

    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={images.Offermage} resizeMode='cover' style={styles.restaurantImageStyle} />
                </View>
                <View style={{ marginLeft: Metrics.CountScale(10), flex: 1 }}>
                    <View style={styles.restaurantNameTextStyle}>
                        <Text style={{ fontSize: Metrics.CountScale(17), flex: 1 }} numberOfLines={1}>
                            {this.props.data.name}
                        </Text>
                    </View>
                    <Text style={{ fontSize: Metrics.CountScale(12), flex: 1 }} numberOfLines={2}>
                        {this.props.data.address}
                    </Text>
                </View>

                {this.props.screenName == 'Visited' &&
                    <TouchableOpacity style={styles.locationIconStyle}>
                        <Image source={images.VisitedLocation} resizeMode={'cover'} />
                    </TouchableOpacity>
                }
                {this.props.screenName == 'Top10' &&
                    <TouchableOpacity style={styles.locationIconStyle}>
                        <Image source={images.BookmarkIcon} resizeMode={'cover'} />
                    </TouchableOpacity>
                }
                {this.props.screenName == 'WantToGo' &&
                    <TouchableOpacity onPress={() => this.removeFromWantToGo(this.props.data.id)} style={styles.locationIconStyle}>
                        <Image source={images.DarkHeartIcon} resizeMode={'cover'} />
                    </TouchableOpacity>
                }
                {this.props.screenName == 'Recommended' &&
                    <TouchableOpacity style={styles.locationIconStyle}>
                        <Image source={images.LikeIcon} resizeMode={'cover'} />
                    </TouchableOpacity>
                }

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: Metrics.CountScale(15),
        flexDirection: 'row',
        flex: 1,
        paddingVertical: Metrics.CountScale(20),
        borderBottomWidth: 1,
        borderBottomColor: Colors.BORDERCOLOR
    },
    restaurantImageStyle: {
        borderRadius: Metrics.CountScale(10),
        width: Metrics.screenWidth / 2.5,
        height: Metrics.commonImageView
    },
    restaurantNameTextStyle: {
        marginTop: Metrics.CountScale(10),
        marginBottom: Metrics.CountScale(5)
    },
    locationIconStyle: {
        alignSelf: 'flex-start',
        paddingHorizontal: Metrics.CountScale(10),
        paddingVertical: Metrics.CountScale(10),
        marginRight: -4
    }
});

export default RestaurantListView;