//LIBRAIRES
import React from 'react';
import { Image, View } from 'react-native';
//ASSETS
import { Metrics, images } from '../../assets/index';

export const BlackBackIcon = () => {
    return (
        <View style={{ height: Metrics.CountScale(40), justifyContent: 'center', paddingLeft: Metrics.CountScale(20), paddingRight: Metrics.CountScale(20) }}>
            <Image source={images.RightBackIcon} resizeMode='stretch' />
        </View>
    )
}

export const WhiteBackIcon = () => {
    return (
        <View style={{ height: Metrics.CountScale(40), justifyContent: 'center', paddingLeft: Metrics.CountScale(20), paddingRight: Metrics.CountScale(20) }}>
            <Image source={images.WhiteBackIcon} resizeMode='stretch' />
        </View>
    )
}





