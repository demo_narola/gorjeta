import React from 'react';
import { Text, View, StyleSheet, Image, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { images, Metrics, Colors } from '../../assets/index';

const users = [
    {
        name: 'Hariom Raje',
    },
    {
        name: 'Rohan Mali',
    },
    {
        name: 'Nitin Ghadiya',
    },
    {
        name: 'Monica Vekariya',
    }
];

class HomeCardView extends React.Component {

    restaurantDetail() {
        this.props.navigate.navigate('RestaurantDetail');
    }

    render() {
        return (
            <TouchableWithoutFeedback onPress={() => this.restaurantDetail()} >
                <View style={{ borderRadius: Metrics.CountScale(15), overflow: 'hidden' }}>
                    <View style={[styles.shadowEffect, { marginBottom: Metrics.CountScale(10), marginTop: Metrics.CountScale(10), marginHorizontal: Metrics.CountScale(10), backgroundColor: Colors.WHITE, borderRadius: Metrics.CountScale(15), width: Metrics.offerViewWidth }]}>
                        <View>
                            <ImageBackground source={images.Offermage} resizeMode='contain' style={{ width: '100%', height: Metrics.offerImage }} >
                                <View style={{ justifyContent: 'flex-end', flex: 1, margin: Metrics.CountScale(13) }}>
                                    <Text style={{ fontSize: Metrics.CountScale(15), color: Colors.WHITE }}>
                                        Jackson Market and Deli
                                    </Text>
                                </View>
                                <View style={styles.recommendListView}>
                                    {users.map((inboxObj, index) => (
                                        <View style={{ width: Metrics.CountScale(50), marginRight: Metrics.CountScale(-27), zIndex: -index + 4 }}>
                                            {index <= 2 &&
                                                <View style={styles.commonAbsolute}>
                                                    <Image source={images.HomeImage} resizeMode={'cover'} style={styles.recommendList} />
                                                </View>
                                            }
                                        </View>
                                    ))}
                                    <View style={{ width: Metrics.CountScale(30), marginRight: Metrics.CountScale(10), zIndex: 1 }}>
                                        {users.length > 3 &&
                                            <View style={[styles.commonAbsolute, styles.shadowEffect, { borderRadius: Metrics.CountScale(30) / 2 }]}>
                                                <Image source={images.SettingsImage} resizeMode={'cover'} style={{ width: Metrics.CountScale(30), height: Metrics.CountScale(30) }} />
                                            </View>
                                        }
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>
                        <View style={{ margin: Metrics.CountScale(15), flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={images.YellowLocation} resizeMode={'cover'} style={{ marginRight: Metrics.CountScale(8) }} />
                            <Text numberOfLines={1} style={{ fontSize: Metrics.CountScale(12) }}>4065, Jackson Avenue, culver city</Text>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    commonAbsolute: {
        position: 'absolute',
        bottom: Metrics.CountScale(0),
        right: Metrics.CountScale(0),
        marginBottom: Metrics.CountScale(-12)
    },
    shadowEffect: {
        shadowOpacity: 0.2,
        shadowOffset: { height: 2 },
        elevation: 5,
        shadowColor: Colors.GRAY
    },
    recommendList: {
        width: Metrics.CountScale(30),
        height: Metrics.CountScale(30),
        borderWidth: 1,
        borderColor: Colors.WHITE,
        borderRadius: Metrics.CountScale(30) / 2
    },
    recommendListView: {
        position: 'absolute',
        bottom: Metrics.CountScale(0),
        right: Metrics.CountScale(0),
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});

export default HomeCardView;