import React, { Component } from "react";
import { NetInfo, View, Text } from "react-native";
import { Metrics, Colors } from '../../assets/index';

export default class InternetConnectivity extends Component {

    setNetworkStatus = status => {
        this.setState({ isConnected: status });
    };

    state = {
        isConnected: true
    };

    componentDidMount() {
        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ isConnected });
        });

        NetInfo.isConnected.addEventListener(
            "connectionChange",
            this.setNetworkStatus
        );
    }

    componentWillUnMount() {
        NetInfo.isConnected.removeEventListener(
            "connectionChange",
            this.setNetworkStatus
        );
    }


    render() {
        return (
            <View>
                {this.state.isConnected == false ?
                    <View style={styles.container}>
                        <Text style={styles.textStyle}>No Internet Connection</Text>
                    </View>
                    : null}
            </View>
        )
    }
}
const styles = {
    container: {
        backgroundColor: Colors.BLACK,
        justifyContent: 'center',
        paddingVertical: Metrics.CountScale(10),
        alignItems: 'center'
    },
    textStyle: {
        color: Colors.WHITE
    }
}