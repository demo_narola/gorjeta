import React from 'react';
import { Text, View, Image, TouchableOpacity, Platform } from 'react-native';
import { BlackBackIcon } from '../common/Header';
import { images, Metrics, Colors } from '../../assets/index';

class ProfileView extends React.Component {

    // state = {
    //     flag: true
    // };

    following() {
        this.props.navigate.navigate('Following');
    }
    followers() {
        this.props.navigate.navigate('Followers');
    }
    visited() {
        this.props.navigate.navigate('Visited');
    }

    back() {
        this.props.navigate.goBack();
    }

    setting() {
        this.props.navigate.navigate('Setting');
    }

    render() {
        return (
            <View style={{ backgroundColor: Colors.NAVIGATIONLABEL, justifyContent: 'flex-end', paddingHorizontal: this.props.flag == true ? Metrics.CountScale(10) : Metrics.CountScale(20) }}>
                <View style={{ flex: 1, paddingTop: Platform.OS == 'ios' ? (Metrics.screenHeight == 812 ? 44 : 20) : 0 }}>
                    {this.props.flag == true &&
                        <TouchableOpacity onPress={() => { this.back() }} style={{ width: Metrics.CountScale(50), marginLeft: Metrics.CountScale(-10), marginBottom: Metrics.CountScale(15) }}>
                            <BlackBackIcon />
                        </TouchableOpacity>
                    }
                    {this.props.flag == false &&
                        <View style={{ height: Metrics.CountScale(40) }} />
                    }
                    <View style={{ flexDirection: 'row', alignItems: 'center', flex: 0.77, justifyContent: 'space-evenly', paddingBottom: this.props.flag == true ? Metrics.CountScale(25) : Metrics.CountScale(40), }}>
                        <View>
                            <Image source={images.ProfileOvalImage} style={{ borderRadius: this.props.flag == true ? Metrics.CountScale(79) / 2 : Metrics.CountScale(84) / 2, width: this.props.flag == true ? Metrics.CountScale(79) : Metrics.CountScale(79), height: this.props.flag == true ? Metrics.CountScale(79) : Metrics.CountScale(84), borderWidth: 2, borderColor: Colors.WHITE }} />
                        </View>
                        <View style={{ flex: 0.8, flexWrap: 'wrap', }}>
                            <Text numberOfLines={1} style={{ fontSize: Metrics.CountScale(23), color: Colors.WHITE, fontWeight: '600' }}>
                                {this.props.data.firstName} {this.props.data.lastName}
                            </Text>
                        </View>
                        <View>
                            {this.props.flag == true &&
                                <TouchableOpacity>
                                    <View style={{ backgroundColor: Colors.WHITE, borderRadius: Metrics.CountScale(6), paddingHorizontal: Metrics.CountScale(23), paddingVertical: Metrics.CountScale(8) }}>
                                        <Text style={{ fontSize: Metrics.CountScale(17) }}>{this.props.data.follow == true ? 'Following' : 'Follow'}</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                            {this.props.flag == false &&
                                <TouchableOpacity onPress={() => this.setting()}>
                                    <View style={{ backgroundColor: Colors.WHITE, borderRadius: Metrics.CountScale(5), padding: Metrics.CountScale(10) }}>
                                        <Image source={images.SettingIcon} resizeMode={'cover'} />
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 0.23, paddingBottom: Metrics.CountScale(30) }}>
                        <TouchableOpacity onPress={() => this.followers()} style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text style={{ fontSize: Metrics.CountScale(22), color: Colors.WHITE, fontWeight: '600' }}>
                                {this.props.data.followers}
                            </Text>
                            <Text style={{ fontSize: Metrics.CountScale(14), color: Colors.WHITE }}>
                                Followers
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.following()} style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text style={{ fontSize: Metrics.CountScale(22), color: Colors.WHITE, fontWeight: '600' }}>
                                {this.props.data.following}
                            </Text>
                            <Text style={{ fontSize: Metrics.CountScale(14), color: Colors.WHITE }}>
                                Following
                                    </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.visited()} style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                            <Text style={{ fontSize: Metrics.CountScale(22), color: Colors.WHITE, fontWeight: '600' }}>
                                {this.props.data.visited}
                            </Text>
                            <Text style={{ fontSize: Metrics.CountScale(14), color: Colors.WHITE }}>
                                Visited
                                </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

// const styles = StyleSheet.create({

// });

export default ProfileView;