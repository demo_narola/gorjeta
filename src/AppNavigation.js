//LIBRARIES
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import React from 'react';
import { Image, View } from 'react-native';
//ASSETS
import Welcome from './screens/Welcome';
import Login from './screens/Login';
import Registration from './screens/Registration';
import ForgotPassword from './screens/ForgotPassword';
import Home from './screens/Home';
import FoodFeed from './screens/FoodFeed';
import Add from './screens/Add';
import Explore from './screens/Explore';
import Profile from './screens/Profile';
import AskForRecommendation from './screens/AskForRecommendation';
import RestaurantDetail from './screens/RestaurantDetail';
import RecommendToAnyUser from './screens/RecommendToAnyUser';
import EvaluationViewAll from './screens/EvaluationViewAll';
import AddMyReview from './screens/AddMyReview';
import Followers from './screens/Followers';
import WantToGo from './screens/WantToGo';
import Reviewed from './screens/Reviewed';
import Following from './screens/Following';
import Visited from './screens/Visited';
import Top10 from './screens/Top10';
import Recommended from './screens/Recommended';
import RecommendedForMe from './screens/RecommendedForMe';
import ChangePassword from './screens/ChangePassword';
import MyProfile from './screens/MyProfile';
import FoodType from './screens/FoodType';
import Setting from './screens/Setting';
import EditProfile from './screens/EditProfile';
import FriendsModule from './screens/FriendsModule';
import ViewAllRating from './screens/ViewAllRating';
import RecommendFollowerList from './screens/RecommendFollowerList';
import MyReview from './screens/MyReview';
import { images, Colors } from './assets/index';

//=======BOTTOM TABS DECLARATION=========//

const HomeStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: () => ({
            header: null
        })
    }
});

const FoodFeedStack = createStackNavigator({
    FoodFeed: { screen: FoodFeed }
});

const AddStack = createStackNavigator({
    Add: {
        screen: Add,
        navigationOptions: () => ({
            header: null
        })
    }
});

const ExploreStack = createStackNavigator({
    Explore: {
        screen: Explore,
        navigationOptions: () => ({
            header: null
        })
    }
});

const ProfileStack = createStackNavigator({
    MyProfile: {
        screen: MyProfile,
        navigationOptions: () => ({
            header: null
        })
    }
});

const TabBar = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: () => ({
                tabBarLabel: 'Home',
                header: null
            })
        },
        FoodFeed: {
            screen: FoodFeedStack,
            navigationOptions: () => ({
                tabBarLabel: 'FoodFeed'
            })
        },
        Add: {
            screen: AddStack,
            navigationOptions: () => ({
                tabBarLabel: <View></View>
            }),
        },
        Explore: {
            screen: ExploreStack,
            navigationOptions: () => ({
                tabBarLabel: 'Explore',
                header: null
            }),
        },
        MyProfile: {
            screen: ProfileStack,
            navigationOptions: () => ({
                tabBarLabel: 'Profile',
                header: null
            }),
        }
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Home') {
                    iconName = focused ? images.HomeActiveIcon : images.HomeIcon;
                } else if (routeName === 'FoodFeed') {
                    iconName = focused ? images.FoodActiveIcon : images.FoodIcon;
                } else if (routeName === 'Add') {
                    return <Image source={images.AddIcon} style={{ marginBottom: 18 }} resizeMode='center' />
                } else if (routeName === 'Explore') {
                    iconName = focused ? images.ExploreActiveIcon : images.ExploreIcon;
                } else if (routeName === 'MyProfile') {
                    iconName = focused ? images.ProfileActiveIcon : images.ProfileIcon;
                }
                return <Image source={iconName} resizeMode='center' />;
            },
        }),
        tabBarOptions: {
            activeTintColor: Colors.NAVIGATIONLABEL
        },
        swipeEnabled: false
    }
);

const MainStack = createStackNavigator({
    Welcome: {
        screen: Welcome,
        navigationOptions: () => ({
            header: null
        })
    },
    Login: {
        screen: Login,
        navigationOptions: () => ({
            header: null,
            headerBackTitle: null
        })
    },
    Registration: {
        screen: Registration,
        navigationOptions: () => ({
            header: null
        })
    },
    ForgotPassword: {
        screen: ForgotPassword
    },
    TabBar: {
        screen: TabBar,
        navigationOptions: () => ({
            header: null,
            headerBackTitle: null
        })
    },
    FoodType: {
        screen: FoodType,
        navigationOptions: () => ({
            header: null
        })
    },
    AskForRecommendation: {
        screen: AskForRecommendation,
        navigationOptions: () => ({
            header: null
        })
    },
    RestaurantDetail: {
        screen: RestaurantDetail,
        navigationOptions: () => ({
            header: null,
            headerBackTitle: null
        })
    },
    RecommendToAnyUser: {
        screen: RecommendToAnyUser
    },
    EvaluationViewAll: {
        screen: EvaluationViewAll
    },
    AddMyReview: {
        screen: AddMyReview
    },
    Followers: {
        screen: Followers
    },
    WantToGo: {
        screen: WantToGo
    },
    Reviewed: {
        screen: Reviewed
    },
    Following: {
        screen: Following
    },
    Visited: {
        screen: Visited
    },
    Top10: {
        screen: Top10
    },
    Recommended: {
        screen: Recommended
    },
    RecommendedForMe: {
        screen: RecommendedForMe
    },
    ChangePassword: {
        screen: ChangePassword
    },
    Setting: {
        screen: Setting,
        navigationOptions: () => ({
            headerBackTitle: null
        })
    },
    FriendsModule: {
        screen: FriendsModule
    },
    ViewAllRating: {
        screen: ViewAllRating
    },
    EditProfile: {
        screen: EditProfile
    },
    RecommendFollowerList: {
        screen: RecommendFollowerList
    },
    MyReview: {
        screen: MyReview
    },
    Profile: {
        screen: Profile,
        navigationOptions: () => ({
            header: null
        })
    }
});

const AppNavigation = createAppContainer(MainStack);

export default AppNavigation;